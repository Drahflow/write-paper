#include "restorer.h"

#include "memory.h"
#include "main.h"

#include <math.h>
#include <stdio.h>

void restoreBackground(Tool *rawTool, float sx, float sy, float ex, float ey, uint32_t pressure, uint64_t dt) {
  if(!rawTool && pressure < dt) printf("."); // ignore parameters

  Restorer *restorer = (Restorer *)rawTool;

  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);
  float size = 40;

  for(float f = 0; f < 1; f += size / 2 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    uint32_t rsx = cx - size < 0? 0: cx - size;
    uint32_t rsy = cy - size < 0? 0: cy - size;
    uint32_t rex = cx + size > FRAMEBUFFER_WIDTH? FRAMEBUFFER_WIDTH: cx + size;
    uint32_t rey = cy + size > FRAMEBUFFER_HEIGHT? FRAMEBUFFER_HEIGHT: cy + size;

    for(uint32_t yy = rsy; yy < rey; ++yy) {
      for(uint32_t xx = rsx; xx < rex; ++xx) {
        float dist2 = (yy - cy) * (yy - cy) + (xx - cx) * (xx - cx);
        if(dist2 >= size * size) continue;

        uint16_t strength = 65535 * sqrtf(size * size - dist2) / size;
        restorer->background->restore(restorer->background, restorer->depthLayer, restorer->spectrumLayer, xx, yy, strength);
      }
    }
  }

  // TODO: A redraw queue would accelerate this significantly
  for(float f = 0; f < 1; f += size / 2 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - size - 1;
    float beginY = cy - size - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + size + 1;
    float endY = cy + size + 1;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    ((Layer *)restorer->spectrumLayer)->renderToFramebuffer((Layer *)restorer->spectrumLayer,
      beginX, beginY, endX - beginX, endY - beginY);
  }
}

Restorer *newRestorer(Background *background, SpectrumLayer *spectrumLayer, DepthLayer *depthLayer) {
  Restorer *restorer = assertMalloc(sizeof(Restorer));
  restorer->background = background;
  restorer->spectrumLayer = spectrumLayer;
  restorer->depthLayer = depthLayer;

  restorer->tool.lower = ignoreTool;
  restorer->tool.raise = ignoreTool;
  restorer->tool.draw = restoreBackground;

  return restorer;
}
