#include "depth.h"
#include "memory.h"
#include "main.h"

void renderToFramebuffer(Layer *rawLayer, uint32_t x, uint32_t y, uint32_t w, uint32_t h) {
  DepthLayer *layer = (DepthLayer *)rawLayer;

  for(uint32_t yy = y; yy < y + h; ++yy) {
    for(uint32_t xx = x; xx < x + w; ++xx) {
      // TODO: Sample into aggregate statistics instead

      uint8_t depth = layer->data[xx + yy * FRAMEBUFFER_WIDTH].depth;
      uint8_t softness = layer->data[xx + yy * FRAMEBUFFER_WIDTH].softness;

      rgba *target = framebuffer + xx + yy * FRAMEBUFFER_WIDTH;
      target->r = depth;
      target->g = depth;

      target->b = softness;
    }
  }
}

DepthLayer *newDepthLayer() {
  DepthLayer *depthLayer = assertMalloc(sizeof(DepthLayer)
      + FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * sizeof(depthInformation));

  depthLayer->layer.renderToFramebuffer = renderToFramebuffer;

  return depthLayer;
}
