#include "ui.h"

#include "png.h"
#include "main.h"
#include "memory.h"
#include "digitizer.h"
#include "touchscreen.h"
#include "tool.h"
#include "pigment_data.h"
#include "spectrum.h"
#include "ballpen.h"
#include "brush.h"
#include "colored_pencil.h"
#include "restorer.h"

#include <string.h>
#include <stdio.h>
#include <math.h>

static rgba resources[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT];

typedef struct {
  uint16_t x, y;
  uint16_t w, h;
} UiImage;

static const UiImage menu = {
  .x = 0,
  .y = 0,
  .w = 456,
  .h = 445
};
static const UiImage ballpen = {
  .x = 500,
  .y = 0,
  .w = 200,
  .h = 200
};
static const UiImage brush = {
  .x = 700,
  .y = 0,
  .w = 200,
  .h = 200
};
static const UiImage coloredPencil = {
  .x = 900,
  .y = 0,
  .w = 200,
  .h = 200
};
static const UiImage arrowLeft = {
  .x = 500,
  .y = 200,
  .w = 200,
  .h = 200
};
static const UiImage eraser = {
  .x = 700,
  .y = 200,
  .w = 200,
  .h = 200
};

void blit(rgba *src, uint16_t sx, uint16_t sy, uint16_t w, uint16_t h, rgba *dstBackup, rgba *dst, uint16_t dx, uint16_t dy) {
  printf("Blitting %d,%d+%d%d to %d,%d\n", sx, sy, w, h, dx, dy);

  if(sx + w >= FRAMEBUFFER_WIDTH || dx + w >= FRAMEBUFFER_WIDTH ||
      sy + h >= FRAMEBUFFER_HEIGHT || dy + h >= FRAMEBUFFER_HEIGHT) {
    fprintf(stderr, "Blit would exceed bounds.\n");
    return;
  }

  for(uint16_t yy = 0; yy < h; ++yy) {
    for(uint16_t xx = 0; xx < w; ++xx) {
      rgba *s = src + sx + xx + (sy + yy) * FRAMEBUFFER_WIDTH;
      rgba *db = dstBackup + dx + xx + (dy + yy) * FRAMEBUFFER_WIDTH;
      rgba *d = dst + dx + xx + (dy + yy) * FRAMEBUFFER_WIDTH;

      d->r = ((uint16_t)db->r * (255 - s->a) + s->r * s->a) >> 8;
      d->g = ((uint16_t)db->g * (255 - s->a) + s->g * s->a) >> 8;
      d->b = ((uint16_t)db->b * (255 - s->a) + s->b * s->a) >> 8;
    }
  }
}

void uiBtnDown(UI *ui, float x, float y) {
  if(!ui->tool) {
    ui->tool = ((Digitizer *)ui->digitizer)->tool;
    ((Digitizer *)ui->digitizer)->tool = NULL;
  }

  ui->menuOpenX = x;
  ui->menuOpenY = y;
  uint16_t mx = x - menu.w / 2;
  uint16_t my = y - menu.h / 2;

  blit(resources, menu.x, menu.y, menu.w, menu.h, drawingBuffer, framebuffer, mx, my);
}

void stopUi(UI *ui) {
  memcpy(framebuffer, drawingBuffer, FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * sizeof(rgba));
  ((Digitizer *)ui->digitizer)->tool = ui->tool;
  ui->tool = NULL;
}

static void clearFramebuffer() {
  for(uint16_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint16_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      framebuffer[x + y * FRAMEBUFFER_WIDTH].r = 245;
      framebuffer[x + y * FRAMEBUFFER_WIDTH].g = 245;
      framebuffer[x + y * FRAMEBUFFER_WIDTH].b = 240;
    }
  }
}

#define TOOL_WIDTH_X_START (FRAMEBUFFER_WIDTH - 600)
static void showToolWidth(UI *ui) {
  for(uint32_t yy = 80; yy < 120; ++yy) {
    for(uint32_t xx = TOOL_WIDTH_X_START; xx < FRAMEBUFFER_WIDTH - 100; ++xx) {
      if(yy == 100 || ((xx - TOOL_WIDTH_X_START) % 100 == 50 && yy >= 98 && yy <= 102)) {
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].r = 0;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].g = 0;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].b = 0;
      } else {
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].r = 255;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].g = 255;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].b = 255;
      }
    }
  }

  uint32_t x = TOOL_WIDTH_X_START + 250 + 250 * logf(((Tool *)ui->tool)->width)/logf(20);
  for(uint32_t yy = 80; yy < 120; ++yy) {
    framebuffer[x + yy * FRAMEBUFFER_WIDTH].r = 0;
    framebuffer[x + yy * FRAMEBUFFER_WIDTH].g = 0;
    framebuffer[x + yy * FRAMEBUFFER_WIDTH].b = 0;
  }
}

static void paintPigment(UI *ui, const SpectrumPigment *pigment, uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
  SpectrumLayer *spectrum = ((SpectrumLayer *)ui->spectrumLayer);
  rgba bufWhite, bufBlack;
  spectrum->layer.renderSpectrumPigment(&spectrum->layer, pigment, &bufWhite, &bufBlack);

  for(uint16_t yy = y; yy < y + h; ++yy) {
    for(uint16_t xx = x; xx < x + w; ++xx) {
      if(xx - x + yy - y < (w + h) / 2) {
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].r = bufWhite.r;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].g = bufWhite.g;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].b = bufWhite.b;
      } else {
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].r = bufBlack.r;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].g = bufBlack.g;
        framebuffer[xx + yy * FRAMEBUFFER_WIDTH].b = bufBlack.b;
      }
    }
  }
}

static const SpectrumPigment **availablePigments = NULL;

void initAvailablePigments() {
  static const SpectrumPigment *initialPigments[] = {
    NULL, NULL,
    &FORS_burntumber, &FORS_rawumber, &FORS_vandykebrown, &FORS_burntsienna,
    &FORS_rawsienna, &FORS_redochre, &FORS_redlead, &FORS_cadmiumred,
    &FORS_alizarin, &FORS_madderlake, &FORS_lacdye, &FORS_carminelake,
    &FORS_vermilion, &FORS_realgar, &FORS_yellowlakeresed, &FORS_massicot,
    &FORS_yellowochre, &FORS_gamboge, &FORS_naplesyellow, &FORS_leadtinyellowII,
    &FORS_leadtinyellowI, &FORS_saffron, &FORS_orpiment, &FORS_cobaltyellow,
    &FORS_cadmiumyellow, &FORS_chromegreen, &FORS_cobaltgreen, &FORS_cadmiumgreen,
    &FORS_greenearth, &FORS_viridian, &FORS_phthalogreen, &FORS_verdigris,
    &FORS_malachite, &FORS_bluebice, &FORS_cobaltblue, &FORS_azurite,
    &FORS_egyptianblue, &FORS_ultramarine, &FORS_Phthaloblue, &FORS_smalt,
    &FORS_indigo, &FORS_mayablue, &FORS_prussianblue, &FORS_cobaltviolet,
    &FORS_ivoryblack, &FORS_vineblack, &FORS_boneblack, &FORS_lampblack,
    &FORS_gypsum, &FORS_chalk, &FORS_leadwhite, &FORS_zincwhite,
    &FORS_titaniumwhite, &FORS_lithopone, &FORS_cardboard,
    &CODE_pitchblack, &CODE_red, &CODE_yellow, &CODE_green, &CODE_cyan, &CODE_blue, &CODE_magenta,
    &CODE_neon_red, &CODE_neon_yellow, &CODE_neon_green, &CODE_neon_cyan, &CODE_neon_blue, &CODE_neon_magenta,
    NULL
  };

  if(availablePigments) return;
  availablePigments = malloc(sizeof(initialPigments));
  for(uint32_t i = 0; i < sizeof(initialPigments) / sizeof(*initialPigments); ++i) {
    availablePigments[i] = initialPigments[i];
  }
}

static void showToolOutput(UI *ui) {
  paintPigment(ui, &((Tool *)ui->tool)->pigment, FRAMEBUFFER_WIDTH / 2 - 100, FRAMEBUFFER_HEIGHT / 2 - 100, 200, 200);
}

#define SPECTRUM_LINE_WIDTH (FRAMEBUFFER_WIDTH / 256)
#define SPECTRUM_X_START (FRAMEBUFFER_WIDTH - 100 - 64 * SPECTRUM_LINE_WIDTH)
#define SPECTRUM_Y_STARTZONE1 (FRAMEBUFFER_HEIGHT / 20 * 0.8)
#define SPECTRUM_Y_START1 (FRAMEBUFFER_HEIGHT / 20 * 1)
#define SPECTRUM_Y_END1 (FRAMEBUFFER_HEIGHT / 20 * 5)
#define SPECTRUM_Y_ENDZONE1 (FRAMEBUFFER_HEIGHT / 20 * 5.2)
#define SPECTRUM_Y_STARTZONE2 (FRAMEBUFFER_HEIGHT / 20 * 5.8)
#define SPECTRUM_Y_START2 (FRAMEBUFFER_HEIGHT / 20 * 6)
#define SPECTRUM_Y_END2 (FRAMEBUFFER_HEIGHT / 20 * 10)
#define SPECTRUM_Y_ENDZONE2 (FRAMEBUFFER_HEIGHT / 20 * 10.2)
#define SPECTRUM_Y_STARTZONE3 (FRAMEBUFFER_HEIGHT / 20 * 10.8)
#define SPECTRUM_Y_START3 (FRAMEBUFFER_HEIGHT / 20 * 11)
#define SPECTRUM_Y_END3 (FRAMEBUFFER_HEIGHT / 20 * 15)
#define SPECTRUM_Y_ENDZONE3 (FRAMEBUFFER_HEIGHT / 20 * 15.2)

static void showSpectrum(UI *ui) {
  SpectrumPigment *pigment = &((Tool *)ui->tool)->pigment;

  const uint32_t HEIGHT = FRAMEBUFFER_HEIGHT / 20 * 4;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    uint32_t reflY = SPECTRUM_Y_START1 + HEIGHT - ((HEIGHT * pigment->bucket[freq].reflectance) >> 16);
    uint32_t transY = SPECTRUM_Y_START2 + HEIGHT - ((HEIGHT * pigment->bucket[freq].transmittance) >> 16);
    uint32_t lumiY = SPECTRUM_Y_START3 + HEIGHT - ((HEIGHT * pigment->bucket[freq].luminosity) >> 16);

    for(uint32_t y = SPECTRUM_Y_START1; y < SPECTRUM_Y_END1; ++y) {
      for(uint32_t x = SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * freq; x < SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * (freq + 1); ++x) {
        framebuffer[x + y * FRAMEBUFFER_WIDTH].r = y < reflY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].g = y < reflY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].b = y < reflY? 255: 0;
      }
    }

    for(uint32_t y = SPECTRUM_Y_START2; y < SPECTRUM_Y_END2; ++y) {
      for(uint32_t x = SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * freq; x < SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * (freq + 1); ++x) {
        framebuffer[x + y * FRAMEBUFFER_WIDTH].r = y < transY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].g = y < transY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].b = y < transY? 255: 0;
      }
    }

    for(uint32_t y = SPECTRUM_Y_START3; y < SPECTRUM_Y_END3; ++y) {
      for(uint32_t x = SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * freq; x < SPECTRUM_X_START + SPECTRUM_LINE_WIDTH * (freq + 1); ++x) {
        framebuffer[x + y * FRAMEBUFFER_WIDTH].r = y < lumiY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].g = y < lumiY? 255: 0;
        framebuffer[x + y * FRAMEBUFFER_WIDTH].b = y < lumiY? 255: 0;
      }
    }
  }
}

static struct {
  Tool tool;

  UI *ui;
} dialogInput;

static void savePigment(UI *ui) {
  uint32_t pigmentCount = 0;
  for(; pigmentCount < 3 || availablePigments[pigmentCount]; ++pigmentCount);
  availablePigments = realloc(availablePigments, (pigmentCount + 2) * sizeof(*availablePigments));
  SpectrumPigment *pigment = malloc(sizeof(SpectrumPigment));
  memcpy(pigment, &((Tool *)ui->tool)->pigment, sizeof(*pigment));
  availablePigments[pigmentCount] = pigment;
  availablePigments[pigmentCount + 1] = NULL;

  uint32_t x = pigmentCount % 8;
  uint32_t y = pigmentCount / 8;

  paintPigment(ui, availablePigments[pigmentCount], x * 64, y * 64, 64, 64);
}

static void stopUiOnCenter(Tool *rawTool, float x, float y) {
  if(!rawTool && x == y) printf("."); // ignore parameters

  if(x > FRAMEBUFFER_WIDTH / 2 - 100 && x < FRAMEBUFFER_WIDTH / 2 + 100 &&
      y > FRAMEBUFFER_HEIGHT / 2 - 100 && y < FRAMEBUFFER_HEIGHT / 2 + 100) {
    stopUi(dialogInput.ui);
  }
}

static void savePigmentOrStop(Tool *rawTool, float x, float y) {
  if(x > FRAMEBUFFER_WIDTH / 2 - 300 && x < FRAMEBUFFER_WIDTH / 2 - 100 &&
      y > FRAMEBUFFER_HEIGHT / 2 - 100 && y < FRAMEBUFFER_HEIGHT / 2 + 100) {
    savePigment(dialogInput.ui);
  }

  stopUiOnCenter(rawTool, x, y);
}

static void mixPigment(Tool *rawTool, float oldX, float oldY, float x, float y, uint32_t pressure, uint64_t dt) {
  if(!rawTool && dt == 17) printf("."); // Ignore parameters

  uint32_t ix = x;
  uint32_t iy = y;

  uint32_t pigmentIndex = ~0u;
  uint32_t pigmentIndexTarget = ix / 64 + iy / 64 * 8;
  for(; pigmentIndex < 3 || availablePigments[pigmentIndex]; ++pigmentIndex) {
    if(pigmentIndex == pigmentIndexTarget) break;
  }
  if(pigmentIndex == ~0u) return;

  if(ix - ix / 64 * 64 > 48 && iy - iy / 64 * 64 > 48 && availablePigments[pigmentIndex]) {
    memcpy(&((Tool *)dialogInput.ui->tool)->pigment, availablePigments[pigmentIndex], sizeof(SpectrumPigment));
  } else {
    float dx = x - oldX;
    float dy = y - oldY;
    float dist = sqrtf(dx * dx + dy * dy);
    uint32_t effect = 6553.5 * (dist / 10) * (pressure / 65535.0);
    if(effect > 65535) effect = 65535;

    if(pigmentIndex == 0) {
      diluteSpectrumPigment(&((Tool *)dialogInput.ui->tool)->pigment, 65535);
    } else if(!availablePigments[pigmentIndex]) {
      diluteSpectrumPigment(&((Tool *)dialogInput.ui->tool)->pigment, effect);
    } else {
      mixSpectrumPigments(&((Tool *)dialogInput.ui->tool)->pigment, availablePigments[pigmentIndex], effect);
    }
  }

  showToolOutput(dialogInput.ui);
  showSpectrum(dialogInput.ui);
}

static void editColor(Tool *rawTool, float oldX, float oldY, float x, float y, uint32_t pressure, uint64_t dt) {
  if(x < 8 * 64) {
    mixPigment(rawTool, oldX, oldY, x, y, pressure, dt);
    return;
  }

  if(x > SPECTRUM_X_START - 64 && x < SPECTRUM_X_START) {
    SpectrumPigment *pigment = &((Tool *)dialogInput.ui->tool)->pigment;

    if(SPECTRUM_Y_STARTZONE1 <= y && y < SPECTRUM_Y_ENDZONE1) {
      uint32_t effect = pressure / 20;

      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        uint32_t refl = pigment->bucket[freq].reflectance + (effect * pigment->bucket[freq].transmittance >> 16);
        uint32_t trans = ((65536 - effect) * pigment->bucket[freq].transmittance) >> 16;

        pigment->bucket[freq].reflectance = refl;
        pigment->bucket[freq].transmittance = trans;
      }
    }

    if(SPECTRUM_Y_STARTZONE2 <= y && y < SPECTRUM_Y_ENDZONE2) {
      uint32_t effect = pressure / 20;

      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        uint32_t refl = ((65536 - effect) * pigment->bucket[freq].reflectance) >> 16;
        uint32_t trans = pigment->bucket[freq].transmittance + (effect * pigment->bucket[freq].reflectance >> 16);

        pigment->bucket[freq].reflectance = refl;
        pigment->bucket[freq].transmittance = trans;
      }
    }

    showToolOutput(dialogInput.ui);
    showSpectrum(dialogInput.ui);
  }

  if(x > SPECTRUM_X_START) {
    SpectrumPigment *pigment = &((Tool *)dialogInput.ui->tool)->pigment;
    uint32_t freq = ((uint32_t)x - SPECTRUM_X_START) / SPECTRUM_LINE_WIDTH;

    if(freq < SPECTRUM) {
      if(SPECTRUM_Y_STARTZONE1 <= y && y <= SPECTRUM_Y_ENDZONE1) {
        pigment->bucket[freq].reflectance = (SPECTRUM_Y_END1 - y) * 65535 / (SPECTRUM_Y_END1 - SPECTRUM_Y_START1);
        if(y < SPECTRUM_Y_START1) pigment->bucket[freq].reflectance = 65535;
        if(y > SPECTRUM_Y_END1) pigment->bucket[freq].reflectance = 0;

        if((uint32_t)pigment->bucket[freq].reflectance + pigment->bucket[freq].transmittance > 65535) {
          pigment->bucket[freq].transmittance = 65535 - pigment->bucket[freq].reflectance;
        }
      } else if(SPECTRUM_Y_STARTZONE2 <= y && y <= SPECTRUM_Y_ENDZONE2) {
        pigment->bucket[freq].transmittance = (SPECTRUM_Y_END2 - y) * 65535 / (SPECTRUM_Y_END2 - SPECTRUM_Y_START2);
        if(y < SPECTRUM_Y_START2) pigment->bucket[freq].transmittance = 65535;
        if(y > SPECTRUM_Y_END2) pigment->bucket[freq].transmittance = 0;

        if((uint32_t)pigment->bucket[freq].reflectance + pigment->bucket[freq].transmittance > 65535) {
          pigment->bucket[freq].reflectance = 65535 - pigment->bucket[freq].transmittance;
        }
      } else if(SPECTRUM_Y_STARTZONE3 <= y && y < SPECTRUM_Y_ENDZONE3) {
        pigment->bucket[freq].luminosity = (SPECTRUM_Y_END3 - y) * 65535 / (SPECTRUM_Y_END3 - SPECTRUM_Y_START3);
        if(y < SPECTRUM_Y_START3) pigment->bucket[freq].luminosity = 65535;
        if(y > SPECTRUM_Y_END3) pigment->bucket[freq].luminosity = 0;
      }

      showToolOutput(dialogInput.ui);
      showSpectrum(dialogInput.ui);
    }
  }
}

static void colorDialog(UI *ui) {
  clearFramebuffer();
  initAvailablePigments();

  for(uint32_t i = 0; i < 3 || availablePigments[i]; ++i) {
    uint32_t x = i % 8;
    uint32_t y = i / 8;

    if(availablePigments[i]) {
      paintPigment(ui, availablePigments[i], x * 64, y * 64, 64, 64);
    }
  }

  blit(resources, arrowLeft.x, arrowLeft.y, arrowLeft.w, arrowLeft.h, framebuffer, framebuffer,
      FRAMEBUFFER_WIDTH / 2 - 300, FRAMEBUFFER_HEIGHT / 2 - 100);
  showToolOutput(ui);
  showSpectrum(ui);

  dialogInput.ui = ui;
  dialogInput.tool.lower = ignoreTool;
  dialogInput.tool.raise = savePigmentOrStop;
  dialogInput.tool.draw = editColor;
  ((Digitizer *)ui->digitizer)->tool = (Tool *)&dialogInput;
}

static void editTool(Tool *rawTool, float oldX, float oldY, float x, float y, uint32_t pressure, uint64_t dt) {
  if(!rawTool && oldX + oldY + pressure + dt < 5) printf("."); // ignore parameters
  UI *ui = dialogInput.ui;
  Tool *oldTool = ui->tool;

  if(x < 200 && y < 200) {
    ui->tool = newBallpen(ui->depthLayer, ui->spectrumLayer);
  } else if(x < 400 && y < 200) {
    ui->tool = newBrush(ui->depthLayer, ui->spectrumLayer);
  } else if(x < 600 && y < 200) {
    ui->tool = newColoredPencil(ui->depthLayer, ui->spectrumLayer);
  } else if(x < 800 && y < 200) {
    ui->tool = newRestorer(ui->backgroundLayer, ui->spectrumLayer, ui->depthLayer);
  }

  if(x > TOOL_WIDTH_X_START && x < TOOL_WIDTH_X_START + 500) {
    uint32_t nx = x - TOOL_WIDTH_X_START;
    if(y > 80 && y < 100) {
      nx = nx / 50 * 50;
    }

    if(y > 80 && y < 120) {
      ((Tool *)ui->tool)->width = expf((nx - 250.0) / 250.0 * logf(20));
      showToolWidth(ui);
    }
  }

  if(ui->tool != oldTool) {
    memcpy(&((Tool *)ui->tool)->pigment, &oldTool->pigment, sizeof(oldTool->pigment));
    ((Tool *)ui->tool)->width = oldTool->width;
    free(oldTool);
  }

  if(x < 200 && y > 500 && y < 700) {
    Tool **finger = &((Touchscreen*)ui->touchscreen)->fingerTool[0];
    if(*finger) free(*finger);
    *finger = NULL;
  } else if(x < 400 && y > 500 && y < 700) {
    Tool **finger = &((Touchscreen*)ui->touchscreen)->fingerTool[0];
    if(*finger) free(*finger);
    *finger = (Tool *)newRestorer(ui->backgroundLayer, ui->spectrumLayer, ui->depthLayer);
  }
}

static void toolDialog(UI *ui) {
  clearFramebuffer();

  blit(resources, ballpen.x, ballpen.y, ballpen.w, ballpen.h, drawingBuffer, framebuffer, 0, 0);
  blit(resources, brush.x, brush.y, brush.w, brush.h, drawingBuffer, framebuffer, 200, 0);
  blit(resources, coloredPencil.x, coloredPencil.y, coloredPencil.w, coloredPencil.h, drawingBuffer, framebuffer, 400, 0);
  blit(resources, eraser.x, eraser.y, eraser.w, eraser.h, drawingBuffer, framebuffer, 600, 0);

  blit(resources, eraser.x, eraser.y, eraser.w, eraser.h, drawingBuffer, framebuffer, 200, 500);

  showToolOutput(ui);
  showToolWidth(ui);

  dialogInput.ui = ui;
  dialogInput.tool.lower = ignoreTool;
  dialogInput.tool.raise = stopUiOnCenter;
  dialogInput.tool.draw = editTool;
  ((Digitizer *)ui->digitizer)->tool = (Tool *)&dialogInput;
}

void uiBtnUp(UI *ui, float x, float y) {
  float dx = x - ui->menuOpenX;
  float dy = y - ui->menuOpenY;
  float dist = sqrtf(dx * dx + dy * dy);

  if(dist > 20) {
    if(dy < 0 && fabs(dx) < 0.5 * fabs(dy)) {
      colorDialog(ui);
      return;
    }
    if(dx > 0 && dy < 0) {
      toolDialog(ui);
      return;
    }
  }

  stopUi(ui);
}

UI *newUI() {
  UI *ui = assertMalloc(sizeof(UI));
  ui->btnDown = uiBtnDown;
  ui->btnUp = uiBtnUp;
  ui->tool = NULL;

  int success = 1;

#ifdef WITHPNG // We better find some other way to load icons
  success = success && readPng("menu.png", menu.x, menu.y, resources);
  success = success && readPng("ballpen.png", ballpen.x, ballpen.y, resources);
  success = success && readPng("brush.png", brush.x, brush.y, resources);
  success = success && readPng("colored-pencil.png", coloredPencil.x, coloredPencil.y, resources);
  success = success && readPng("arrow-left.png", arrowLeft.x, arrowLeft.y, resources);
  success = success && readPng("eraser.png", eraser.x, eraser.y, resources);
#endif

  if(!success) return NULL;
  return ui;
}
