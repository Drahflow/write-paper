#ifndef H_B38A994E_37B9_4F15_9878_0DCFB88AF35F
#define H_B38A994E_37B9_4F15_9878_0DCFB88AF35F

#include <stdint.h>

uint8_t setBacklight(uint16_t brightness);

#endif
