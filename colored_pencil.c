#define _GNU_SOURCE

#include "colored_pencil.h"

#include "memory.h"
#include "main.h"
#include "random.h"

#include <stdio.h>
#include <math.h>
#include <strings.h>

#define GENERATION_OVERLAP_PROTECTION 8

static void lower(Tool *rawTool, float x, float y) {
  ColoredPencil *pencil = (ColoredPencil *)rawTool;

  // Start at paper surface
  pencil->z = (pencil->depth->data[(uint16_t)x + (uint16_t)y * FRAMEBUFFER_WIDTH].depth) * (1ull << 24);
  pencil->dz = 0;
  pencil->generation = GENERATION_OVERLAP_PROTECTION;
}

static void raise(Tool *rawTool, float x, float y) {
  if(rawTool && x == y && x == -7) printf("."); // ignore parameters

  bzero(paintGeneration, sizeof(paintGeneration));
}

static void draw(Tool *rawTool, float sx, float sy, float ex, float ey, uint32_t pressure, uint64_t dt) {
  ColoredPencil *pencil = (ColoredPencil *)rawTool;
  pressure = powf(pressure, 0.47) * 255;

  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);
  float width = pressure / 20000.0 * pencil->tool.width;

  uint32_t ddz = pressure * dt / dist;
  ddz = ddz >> 6;

  uint32_t vx = fabs(dx / dist) * 65535;
  uint32_t vy = fabs(dy / dist) * 65535;

  uint32_t opacity = 2048 + pressure * 1.2;
  if(opacity > 65535) opacity = 65535;

  ++pencil->generation;

  for(float f = 0; f < 1; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    pencil->dz += ddz;
    if(pencil->dz > 0 && pencil->z < (uint32_t)pencil->dz) {
      pencil->z = 0;
      pencil->dz = 0;
    } else {
      pencil->z -= pencil->dz;
    }

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 2;
    float endY = cy + width + 2;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    uint32_t pushedZ = pencil->z;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        if(pencil->generation - paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] < GENERATION_OVERLAP_PROTECTION) {
          paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] = pencil->generation;
          continue;
        }

        float pdx = xx - cx;
        float pdy = yy - cy;
        float orthogonalDist = (pdx * dy - pdy * dx) / dist;
        if(fabs(orthogonalDist) > width + 1) continue;
        if(sqrtf(pdx * pdx + pdy * pdy) > width + 1) continue;
        if(pdx * dx + pdy * dy > 0) continue;

        uint32_t borderIntensity = fabs(orthogonalDist) <= width? 65535: 65535 * (1 + width - fabs(orthogonalDist));

        depthInformation *d = pencil->depth->data + xx + yy * FRAMEBUFFER_WIDTH;
        uint32_t paperZ = d->depth << 24;

        // printf("z: %u, lz: %u, pz: %u, bs: %u\n", z, localZ, paperZ, ballShape);

        uint32_t hitIntensity = 65535;
        if(pencil->z > paperZ) hitIntensity = 0;
        if(paperZ > pushedZ) pushedZ = paperZ;

        uint32_t intensity = (vx * d->xRough + vy * d->yRough) >> 6;
        intensity = (intensity * hitIntensity) >> 16;
        if(pressure > 40000) {
          d->depth = pencil->z >> 24;
          intensity += pressure - 40000;
        }

        if(intensity > 65535) intensity = 65535;
        intensity = (intensity * borderIntensity) >> 16;

        paintSpectrumPigment(
            pencil->spectrum, xx, yy,
            &pencil->tool.pigment,
            65535,
            intensity
        );

        paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] = pencil->generation;
      }
    }

    // printf("z: %u, dz: %u, ddz: %u, pz: %u, vx: %u, vy: %u\n", z, dz, ddz, pushedZ, vx, vy);

    if(pencil->z < pushedZ) {
      pencil->z = pushedZ;
      if(pencil->dz > 0) pencil->dz = -pencil->dz;
    }
  }

  for(float f = 0; f < 1; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 2;
    float endY = cy + width + 2;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    ((Layer *)pencil->spectrum)->renderToFramebuffer((Layer *)pencil->spectrum, beginX, beginY, endX - beginX, endY - beginY);
  }
}

ColoredPencil *newColoredPencil(DepthLayer *depth, SpectrumLayer *spectrum) {
  ColoredPencil *pencil = assertMalloc(sizeof(ColoredPencil));
  pencil->depth = depth;
  pencil->spectrum = spectrum;

  pencil->tool.lower = lower;
  pencil->tool.raise = raise;
  pencil->tool.draw = draw;

  return pencil;
}
