#include "spectrum.h"

#include "main.h"
#include "memory.h"
#include "srgb.h"
#include "layer.h"
#include "random.h"

#include <stdio.h>
#include <math.h>
#include <string.h>

static const uint16_t SUNLIGHT[SPECTRUM] = { // 10000 == 1W/m^2/nm
  12729, 13177, 13477, 13873, 14225, 14610, 14867, 15241, 15572, 15204,
  16105, 16393, 16685, 16889, 17167, 17539, 17833, 17705, 18411, 18484,
  18210, 18231, 18610, 18618, 18443, 18898, 19068, 18505, 17470, 18976,
  19430, 18894, 19571, 19507, 18252, 20358, 20461, 20238, 19750, 20363,
  20263, 20090, 20647, 19311, 18910, 17485, 17070, 14140, 16857, 17370,
  17110, 17660, 17567, 16687, 17146, 17686, 13476, 9984, 11595, 10919,
  10250, 9166, 12496, 11623
};

static const uint16_t S_CONE_SENSITIVITY[SPECTRUM] = { // 65535 == 1.0
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 1, 2, 5, 11,
  24, 48, 78, 128, 270, 530, 825, 1270, 1913, 2805,
  3985, 5825, 9285, 13884, 19026, 22303, 25580, 33842, 42359, 48382,
  51556, 56375, 62611, 64978, 64962, 64946, 59215, 52595, 44201, 39913,
  35626, 24992, 20131, 15270, 8024, 5868, 3712, 1561, 1093, 625,
  312, 156, 78, 0
};

static const uint16_t M_CONE_SENSITIVITY[SPECTRUM] = { // 65535 == 1.0
  0, 2, 4, 9, 20, 42, 90, 190, 402, 857,
  1428, 2487, 4839, 7644, 11845, 17358, 24433, 32282, 40168, 48514,
  55269, 60144, 62689, 64040, 65286, 63494, 61320, 58034, 53516, 47129,
  40338, 33788, 25716, 19898, 17567, 16508, 15450, 13460, 11527, 9472,
  7619, 6435, 5704, 4972, 4609, 4245, 3396, 2585, 1937, 1678,
  1419, 952, 764, 576, 308, 228, 148, 62, 43, 24,
  12, 6, 3, 0
};

static const uint16_t L_CONE_SENSITIVITY[SPECTRUM] = { // 65535 == 1.0
  11, 35, 73, 154, 327, 676, 1412, 2807, 5292, 9498,
  13977, 19452, 28856, 36321, 43793, 50796, 56358, 60795, 63078, 65031,
  65354, 64318, 63289, 61615, 58984, 54112, 50503, 46183, 41192, 35159,
  29076, 23573, 17100, 12553, 10744, 9964, 9184, 7785, 6519, 5287,
  4241, 3626, 3267, 2945, 2791, 2638, 2235, 1847, 1502, 1355,
  1208, 877, 724, 571, 316, 237, 157, 68, 48, 27,
  13, 6, 3, 0
};

#define SCALE_SHIFT_BITS_LAST 8

static void renderSpectrumPigment(Layer *rawLayer, const SpectrumPigment *pigment, rgba *dstWhite, rgba *dstBlack) {
  SpectrumLayer *layer = (SpectrumLayer*)rawLayer;

  const uint16_t scaleFactor = layer->scaleFactor;
  const uint8_t partialScaleShift = layer->scaleShift - SCALE_SHIFT_BITS_LAST;
  const uint32_t blackOffset = layer->blackOffset;

  int64_t sWhite = 0, sBlack = 0;
  int64_t mWhite = 0, mBlack = 0;
  int64_t lWhite = 0, lBlack = 0;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    const uint32_t reflectance = pigment->bucket[freq].reflectance;
    const uint32_t transmittance = pigment->bucket[freq].transmittance;
    const uint32_t luminosity = pigment->bucket[freq].luminosity;

    uint32_t intensity = (reflectance * SUNLIGHT[freq]) >> 16;
    intensity += (((transmittance * transmittance) >> 16) * SUNLIGHT[freq]) >> 16;
    intensity += luminosity;

    sWhite += (intensity * S_CONE_SENSITIVITY[freq]) >> partialScaleShift;
    mWhite += (intensity * M_CONE_SENSITIVITY[freq]) >> partialScaleShift;
    lWhite += (intensity * L_CONE_SENSITIVITY[freq]) >> partialScaleShift;

    intensity = (reflectance * SUNLIGHT[freq]) >> 16;
    intensity += luminosity;

    sBlack += (intensity * S_CONE_SENSITIVITY[freq]) >> partialScaleShift;
    mBlack += (intensity * M_CONE_SENSITIVITY[freq]) >> partialScaleShift;
    lBlack += (intensity * L_CONE_SENSITIVITY[freq]) >> partialScaleShift;
  }

  // We now have human eye cone response amplitudes (in s, m, l) and must now find (r, g, b) values
  // such that the same responses are induced. See rgb_spectrum.py for derivation of the coefficients.

  sWhite = (sWhite * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
  mWhite = (mWhite * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
  lWhite = (lWhite * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
  sBlack = (sBlack * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
  mBlack = (mBlack * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
  lBlack = (lBlack * scaleFactor) >> SCALE_SHIFT_BITS_LAST;

  int32_t rWhite = ( 3433  * sWhite + -37050  * mWhite +  39874  * lWhite) >> 16;
  int32_t gWhite = ( -5539 * sWhite +  20803  * mWhite +  -5618  * lWhite) >> 16;
  int32_t bWhite = ( 12506 * sWhite +  -1525  * mWhite +    293  * lWhite) >> 16;
  int32_t rBlack = ( 3433  * sBlack + -37050  * mBlack +  39874  * lBlack) >> 16;
  int32_t gBlack = ( -5539 * sBlack +  20803  * mBlack +  -5618  * lBlack) >> 16;
  int32_t bBlack = ( 12506 * sBlack +  -1525  * mBlack +    293  * lBlack) >> 16;

  rWhite += blackOffset;
  gWhite += blackOffset;
  bWhite += blackOffset;
  rBlack += blackOffset;
  gBlack += blackOffset;
  bBlack += blackOffset;

  // This color lies out of display range :(
  if(rWhite < 0) { rWhite = 0; }
  if(rWhite > 65535) { rWhite = 65535; }
  if(gWhite < 0) { gWhite = 0; }
  if(gWhite > 65535) { gWhite = 65535; }
  if(bWhite < 0) { bWhite = 0; }
  if(bWhite > 65535) { bWhite = 65535; }
  if(rBlack < 0) { rBlack = 0; }
  if(rBlack > 65535) { rBlack = 65535; }
  if(gBlack < 0) { gBlack = 0; }
  if(gBlack > 65535) { gBlack = 65535; }
  if(bBlack < 0) { bBlack = 0; }
  if(bBlack > 65535) { bBlack = 65535; }

  // We now have r, g, b in linear terms on a scale 0 - 65535. We must still map to sRGB via sRGBGammaInverse.

  dstWhite->r = sRGBGammaInverse[rWhite];
  dstWhite->g = sRGBGammaInverse[gWhite];
  dstWhite->b = sRGBGammaInverse[bWhite];
  dstBlack->r = sRGBGammaInverse[rBlack];
  dstBlack->g = sRGBGammaInverse[gBlack];
  dstBlack->b = sRGBGammaInverse[bBlack];
}

static void renderToFramebuffer(Layer *rawLayer, uint32_t x, uint32_t y, uint32_t w, uint32_t h) {
  SpectrumLayer *layer = (SpectrumLayer*)rawLayer;

  const uint16_t scaleFactor = layer->scaleFactor;
  const uint8_t partialScaleShift = layer->scaleShift - SCALE_SHIFT_BITS_LAST;
  const uint32_t blackOffset = layer->blackOffset;

  // TODO: Adjust /sys/class/backlight/pwm-backlight/brightness based on blackOffset and brightness,
  //       i.e. upscale RGB output range and downscale backlight if that allows better colors.

  int64_t minS = 1ull << 62;
  int64_t maxS = -1ull << 62;
  int64_t minL = 1ull << 62;
  int64_t maxL = -1ull << 62;
  int64_t minM = 1ull << 62;
  int64_t maxM = -1ull << 62;
  int64_t minR = 1ull << 62;
  int64_t maxR = -1ull << 62;
  int64_t minG = 1ull << 62;
  int64_t maxG = -1ull << 62;
  int64_t minB = 1ull << 62;
  int64_t maxB = -1ull << 62;

  for(uint32_t yy = y; yy < y + h; ++yy) {
    for(uint32_t xx = x; xx < x + w; ++xx) {
      const uint32_t index = xx + yy * FRAMEBUFFER_WIDTH;
      if(!(layer->dirtyBitmap[index / 8] & (1 << (index % 8)))) continue;
      layer->dirtyBitmap[index / 8] &= ~(1 << (index % 8));

      spectrum *source = layer->data + index;

      int64_t s = 0;
      int64_t m = 0;
      int64_t l = 0;

      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        const uint32_t reflectance = source->bucket[freq].reflectance;
        const uint32_t luminosity = source->bucket[freq].luminosity;

        uint32_t intensity = (reflectance * SUNLIGHT[freq]) >> 16;
        intensity += luminosity;

        s += (intensity * S_CONE_SENSITIVITY[freq]) >> partialScaleShift;
        m += (intensity * M_CONE_SENSITIVITY[freq]) >> partialScaleShift;
        l += (intensity * L_CONE_SENSITIVITY[freq]) >> partialScaleShift;
      }

      // We now have human eye cone response amplitudes (in s, m, l) and must now find (r, g, b) values
      // such that the same responses are induced. See rgb_spectrum.py for derivation of the coefficients.

      s = (s * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
      m = (m * scaleFactor) >> SCALE_SHIFT_BITS_LAST;
      l = (l * scaleFactor) >> SCALE_SHIFT_BITS_LAST;

      int32_t r = ( 3433  * s +  -37050  * m +  39874  * l) >> 16;
      int32_t g = ( -5539  * s +  20803  * m +  -5618  * l) >> 16;
      int32_t b = ( 12506  * s +  -1525  * m +  293  * l) >> 16;

      r += blackOffset;
      g += blackOffset;
      b += blackOffset;

      if(s < minS) { minS = s; } if(s > maxS) { maxS = s; }
      if(l < minL) { minL = l; } if(l > maxL) { maxL = l; }
      if(m < minM) { minM = m; } if(m > maxM) { maxM = m; }
      if(r < minR) { minR = r; } if(r > maxR) { maxR = r; }
      if(g < minG) { minG = g; } if(g > maxG) { maxG = g; }
      if(b < minB) { minB = b; } if(b > maxB) { maxB = b; }

      // This color lies out of display range :(
      if(r < 0) { r = 0; }
      if(r > 65535) { r = 65535; }
      if(g < 0) { g = 0; }
      if(g > 65535) { g = 65535; }
      if(b < 0) { b = 0; }
      if(b > 65535) { b = 65535; }

      // We now have r, g, b in linear terms on a scale 0 - 65535. We must still map to sRGB via sRGBGammaInverse.

      rgba *fbTarget = framebuffer + xx + yy * FRAMEBUFFER_WIDTH;
      rgba *dbTarget = drawingBuffer + xx + yy * FRAMEBUFFER_WIDTH;
      dbTarget->r = fbTarget->r = sRGBGammaInverse[r];
      dbTarget->g = fbTarget->g = sRGBGammaInverse[g];
      dbTarget->b = fbTarget->b = sRGBGammaInverse[b];
    }
  }

  if(x == 0 && y == 0 && w == FRAMEBUFFER_WIDTH && h == FRAMEBUFFER_HEIGHT) {
    printf("Framebuffer blit complete. s:[%lld %lld], m:[%lld %lld], l[%lld %lld], r[%lld %lld], g[%lld %lld], b[%lld %lld]\n",
        (long long int)minS, (long long int)maxS, (long long int)minM, (long long int)maxM, (long long int)minL, (long long int)maxL,
        (long long int)minR, (long long int)maxR, (long long int)minG, (long long int)maxG, (long long int)minB, (long long int)maxB);
  }
}

SpectrumLayer *newSpectrumLayer() {
  SpectrumLayer *spectrumLayer = assertMalloc(sizeof(SpectrumLayer) + FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * sizeof(spectrum));
  uint8_t *dirtyBitmap = assertMalloc(FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT / 8);
  for(uint32_t i = 0; i < FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT / 8; ++i) dirtyBitmap[i] = 0xff;

  spectrumLayer->layer.renderToFramebuffer = renderToFramebuffer;
  spectrumLayer->layer.renderSpectrumPigment = renderSpectrumPigment;

  spectrumLayer->dirtyBitmap = dirtyBitmap;
  // spectrumLayer->blackOffset = 8046; <== apparently correct value to display all visible colors (at horrible contrast)
  spectrumLayer->blackOffset = 500;
  spectrumLayer->scaleShift = 27;
  spectrumLayer->scaleFactor = 2823;

  return spectrumLayer;
}

void mixSpectrumPigments(SpectrumPigment *dst, const SpectrumPigment *src, uint16_t strength16) {
  if(strength16 == 0) return;
  if(strength16 == 65535) {
    memcpy(dst, src, sizeof(SpectrumPigment));
    return;
  }

  const float strength = strength16 / 65535.0;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    // printf("Frequency %d under way\n", freq);

    double srcReflectance = src->bucket[freq].reflectance / 65535.0;
    double srcTransmittance = src->bucket[freq].transmittance / 65535.0;
    double srcLuminosity = src->bucket[freq].luminosity / 65535.0;
    double dstReflectance = dst->bucket[freq].reflectance / 65535.0;
    double dstTransmittance = dst->bucket[freq].transmittance / 65535.0;
    double dstLuminosity = dst->bucket[freq].luminosity / 65535.0;

    // if(freq == 30) printf("Mixing %lf,%lf,%lf (%d) into %lf,%lf,%lf\n",
    //     srcReflectance, srcTransmittance, srcLuminosity, strength16, dstReflectance, dstTransmittance, dstLuminosity);

    // Above values are valid for a 65535 thick layer
    // Let's estimate instead for 6553.5 thick layer
    // transmittance is about geometric in thickness
    srcTransmittance = pow(srcTransmittance, 0.1);
    dstTransmittance = pow(dstTransmittance, 0.1);

    // total reflectance = 1st-layer + transmit^2 * 2nd-layer + ...
    //                   = layer * (1 + transmit^2 + transmit^4 ... )
    srcReflectance = srcReflectance / (
        1 + pow(srcTransmittance, 2) + pow(srcTransmittance, 4) + pow(srcTransmittance, 6) + pow(srcTransmittance, 8) +
        pow(srcTransmittance, 10) + pow(srcTransmittance, 12) + pow(srcTransmittance, 14) + pow(srcTransmittance, 16) +
        pow(srcTransmittance, 18));
    dstReflectance = dstReflectance / (
        1 + pow(dstTransmittance, 2) + pow(dstTransmittance, 4) + pow(dstTransmittance, 6) + pow(dstTransmittance, 8) +
        pow(dstTransmittance, 10) + pow(dstTransmittance, 12) + pow(dstTransmittance, 14) + pow(dstTransmittance, 16) +
        pow(dstTransmittance, 18));

    // if(freq == 30) printf("Mixing (1layer) %lf,%lf,%lf (%d) into %lf,%lf,%lf\n",
    //     srcReflectance, srcTransmittance, srcLuminosity,
    //     strength16,
    //     dstReflectance, dstTransmittance, dstLuminosity);

    // I have no idea how to solve this analytically, so let's simulate photon densities for a few steps
    // through randomly mixed layers.
    float reflected = 0;
    float transmitted = 0;
    float absorbed = 0;

    float forward[10];
    float backward[10];
    for(uint32_t l = 0; l < 10; ++l) {
      forward[l] = 0;
      backward[l] = 0;
    }
    forward[0] = 65536;
    
    if(srcReflectance + srcTransmittance >= 1) srcReflectance = 1 - srcTransmittance;
    if(dstReflectance + dstTransmittance >= 1) dstReflectance = 1 - dstTransmittance;

    // This is actually (transition-)matrix exponentiation. There should be (massively) faster ways.
    for(uint32_t s = 0; s < 100; ++s) {
      // if(freq == 30) {
      //   printf("Light forward %f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
      //       forward[0], forward[1], forward[2], forward[3], forward[4],
      //       forward[5], forward[6], forward[7], forward[8], forward[9]);
      //   printf("Light backward %f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
      //       backward[0], backward[1], backward[2], backward[3], backward[4],
      //       backward[5], backward[6], backward[7], backward[8], backward[9]);
      // }

      float newForward[10];
      float newBackward[10];
      for(uint32_t l = 0; l < 10; ++l) {
        newForward[l] = 0;
        newBackward[l] = 0;
      }

      for(uint32_t l = 0; l < 10; ++l) {
        if(l == 0) {
          reflected += forward[l] * (strength * srcReflectance + (1 - strength) * dstReflectance);
          newForward[l + 1] += forward[l] * (strength * srcTransmittance + (1 - strength) * dstTransmittance);
          newForward[l + 1] += backward[l + 1] * (strength * srcReflectance + (1 - strength) * dstReflectance);
          reflected += backward[l + 1] * (strength * srcTransmittance + (1 - strength) * dstTransmittance);

          absorbed += (forward[l] + backward[l + 1]) * (
              strength * (1 - srcReflectance - srcTransmittance) +
              (1 - strength) * (1 - dstReflectance - dstTransmittance));
        } else if(l == 9) {
          newBackward[l] += forward[l] * (strength * srcReflectance + (1 - strength) * dstReflectance);
          transmitted += forward[l] * (strength * srcTransmittance + (1 - strength) * dstTransmittance);

          absorbed += (forward[l]) * (
              strength * (1 - srcReflectance - srcTransmittance) +
              (1 - strength) * (1 - dstReflectance - dstTransmittance));
        } else {
          newBackward[l] += forward[l] * (strength * srcReflectance + (1 - strength) * dstReflectance);
          newForward[l + 1] += forward[l] * (strength * srcTransmittance + (1 - strength) * dstTransmittance);
          //newForward[l + 1] += backward[l + 1] * (strength * srcReflectance + (1 - strength) * dstReflectance);
          newBackward[l] += backward[l + 1] * (strength * srcTransmittance + (1 - strength) * dstTransmittance);

          absorbed += (forward[l] + backward[l + 1]) * (
              strength * (1 - srcReflectance - srcTransmittance) +
              (1 - strength) * (1 - dstReflectance - dstTransmittance));
        }
      }

      memcpy(forward, newForward, sizeof(forward));
      memcpy(backward, newBackward, sizeof(backward));
    }

    // Source layer only (as refererenc)
    float srcEmitted = 0;
    float srcEmittedAbsorbed = 0;

    for(uint32_t l = 0; l < 10; ++l) {
      forward[l] = 0;
      backward[l] = 0;
    }
    
    for(uint32_t s = 0; s < 100; ++s) {
      float newForward[10];
      float newBackward[10];
      for(uint32_t l = 0; l < 10; ++l) {
        newForward[l] = 0;
        newBackward[l] = 0;
      }

      for(uint32_t l = 0; l < 10; ++l) {
        // src layer
        if(l == 0) {
          srcEmitted += (forward[l] * srcReflectance);
          srcEmitted += (backward[l] * srcTransmittance);
          srcEmitted += srcLuminosity;
        } else {
          newBackward[l - 1] += (forward[l] * srcReflectance);
          newBackward[l - 1] += (backward[l] * srcTransmittance);
          newBackward[l - 1] += srcLuminosity;
        }
        if(l != 9) {
          newForward[l + 1] += (forward[l] * srcTransmittance);
          newForward[l + 1] += (backward[l] * srcReflectance);
          newForward[l + 1] += srcLuminosity;
        }
        srcEmittedAbsorbed += strength * (forward[l] + backward[l]) * (1 - srcReflectance - srcTransmittance);
      }

      memcpy(forward, newForward, sizeof(forward));
      memcpy(backward, newBackward, sizeof(backward));
    }

    // Destination layer only (as reference)
    float dstEmitted = 0;
    float dstEmittedAbsorbed = 0;

    for(uint32_t l = 0; l < 10; ++l) {
      forward[l] = 0;
      backward[l] = 0;
    }
    
    for(uint32_t s = 0; s < 100; ++s) {
      float newForward[10];
      float newBackward[10];
      for(uint32_t l = 0; l < 10; ++l) {
        newForward[l] = 0;
        newBackward[l] = 0;
      }

      for(uint32_t l = 0; l < 10; ++l) {
        if(l == 0) {
          dstEmitted += forward[l] * dstReflectance;
          dstEmitted += backward[l] * dstTransmittance;
          dstEmitted += dstLuminosity;
        } else {
          newBackward[l - 1] += forward[l] * dstReflectance;
          newBackward[l - 1] += backward[l] * dstTransmittance;
          newBackward[l - 1] += dstLuminosity;
        }
        if(l != 9) {
          newForward[l + 1] += forward[l] * dstTransmittance;
          newForward[l + 1] += backward[l] * dstReflectance;
          newForward[l + 1] += dstLuminosity;
        }
        dstEmittedAbsorbed += (1 - strength) * (forward[l] + backward[l]) * (1 - dstReflectance - dstTransmittance);
      }

      memcpy(forward, newForward, sizeof(forward));
      memcpy(backward, newBackward, sizeof(backward));
    }

    // Both layers, src illumination only
    float srcIllumEmitted = 0;
    float srcIllumEmittedAbsorbed = 0;

    for(uint32_t l = 0; l < 10; ++l) {
      forward[l] = 0;
      backward[l] = 0;
    }
    
    for(uint32_t s = 0; s < 100; ++s) {
      float newForward[10];
      float newBackward[10];
      for(uint32_t l = 0; l < 10; ++l) {
        newForward[l] = 0;
        newBackward[l] = 0;
      }

      for(uint32_t l = 0; l < 10; ++l) {
        // src layer
        if(l == 0) {
          srcIllumEmitted += strength * (forward[l] * srcReflectance);
          srcIllumEmitted += strength * (backward[l] * srcTransmittance);
          srcIllumEmitted += strength * srcLuminosity;
        } else {
          newBackward[l - 1] += strength * (forward[l] * srcReflectance);
          newBackward[l - 1] += strength * (backward[l] * srcTransmittance);
          newBackward[l - 1] += strength * srcLuminosity;
        }
        if(l != 9) {
          newForward[l + 1] += strength * (forward[l] * srcTransmittance);
          newForward[l + 1] += strength * (backward[l] * srcReflectance);
          newForward[l + 1] += strength * srcLuminosity;
        }
        srcIllumEmittedAbsorbed += strength * (forward[l] + backward[l]) * (1 - srcReflectance - srcTransmittance);

        // dst layer
        if(l == 0) {
          srcIllumEmitted += (1 - strength) * forward[l] * dstReflectance;
          srcIllumEmitted += (1 - strength) * backward[l] * dstTransmittance;
        } else {
          newBackward[l - 1] += (1 - strength) * forward[l] * dstReflectance;
          newBackward[l - 1] += (1 - strength) * backward[l] * dstTransmittance;
        }
        if(l != 9) {
          newForward[l + 1] += (1 - strength) * forward[l] * dstTransmittance;
          newForward[l + 1] += (1 - strength) * backward[l] * dstReflectance;
        }
        srcIllumEmittedAbsorbed += (1 - strength) * (forward[l] + backward[l]) * (1 - dstReflectance - dstTransmittance);
      }

      memcpy(forward, newForward, sizeof(forward));
      memcpy(backward, newBackward, sizeof(backward));
    }

    // Both layers, dst illumination only
    float dstIllumEmitted = 0;
    float dstIllumEmittedAbsorbed = 0;

    for(uint32_t l = 0; l < 10; ++l) {
      forward[l] = 0;
      backward[l] = 0;
    }
    
    for(uint32_t s = 0; s < 100; ++s) {
      float newForward[10];
      float newBackward[10];
      for(uint32_t l = 0; l < 10; ++l) {
        newForward[l] = 0;
        newBackward[l] = 0;
      }

      for(uint32_t l = 0; l < 10; ++l) {
        // src layer
        if(l == 0) {
          dstIllumEmitted += strength * (forward[l] * srcReflectance);
          dstIllumEmitted += strength * (backward[l] * srcTransmittance);
        } else {
          newBackward[l - 1] += strength * (forward[l] * srcReflectance);
          newBackward[l - 1] += strength * (backward[l] * srcTransmittance);
        }
        if(l != 9) {
          newForward[l + 1] += strength * (forward[l] * srcTransmittance);
          newForward[l + 1] += strength * (backward[l] * srcReflectance);
        }
        dstIllumEmittedAbsorbed += strength * (forward[l] + backward[l]) * (1 - srcReflectance - srcTransmittance);

        // dst layer
        if(l == 0) {
          dstIllumEmitted += (1 - strength) * forward[l] * dstReflectance;
          dstIllumEmitted += (1 - strength) * backward[l] * dstTransmittance;
          dstIllumEmitted += (1 - strength) * dstLuminosity;
        } else {
          newBackward[l - 1] += (1 - strength) * forward[l] * dstReflectance;
          newBackward[l - 1] += (1 - strength) * backward[l] * dstTransmittance;
          newBackward[l - 1] += (1 - strength) * dstLuminosity;
        }
        if(l != 9) {
          newForward[l + 1] += (1 - strength) * forward[l] * dstTransmittance;
          newForward[l + 1] += (1 - strength) * backward[l] * dstReflectance;
          newForward[l + 1] += (1 - strength) * dstLuminosity;
        }
        dstIllumEmittedAbsorbed += (1 - strength) * (forward[l] + backward[l]) * (1 - dstReflectance - dstTransmittance);
      }

      memcpy(forward, newForward, sizeof(forward));
      memcpy(backward, newBackward, sizeof(backward));
    }

    float srcScale = (srcIllumEmitted / (srcIllumEmitted + srcIllumEmittedAbsorbed)) /
      (srcEmitted / (srcEmitted + srcEmittedAbsorbed));
    float dstScale = (dstIllumEmitted / (dstIllumEmitted + dstIllumEmittedAbsorbed)) /
      (dstEmitted / (dstEmitted + dstEmittedAbsorbed));
    
    // if(freq == 30) printf("luminosity src %f,%f vs. %f,%f (scale %f); dst %f,%f vs. %f,%f (scale %f)\n",
    //     srcEmitted, srcEmittedAbsorbed, srcIllumEmitted, srcIllumEmittedAbsorbed, srcScale,
    //     dstEmitted, dstEmittedAbsorbed, dstIllumEmitted, dstIllumEmittedAbsorbed, dstScale);
    if(srcEmitted < 0.00001 || srcIllumEmitted < 0.00001) srcScale = 0;
    if(dstEmitted < 0.00001 || dstIllumEmitted < 0.00001) dstScale = 0;

    float luminosity = srcLuminosity * srcScale + dstLuminosity * dstScale;

    if(freq == 30) printf("Result %f,%f,%f,%f\n",
        reflected, transmitted, absorbed, luminosity);

    float refl = 65536 * reflected / (reflected + transmitted + absorbed);
    float trans = 65536 * transmitted / (reflected + transmitted + absorbed);
    float lumi = 65536 * luminosity;
    if(refl > 65535) refl = 65535;
    if(trans > 65535) trans = 65535;
    if(lumi > 65535) lumi = 65535;
    if(refl < 0) refl = 0;
    if(trans < 0) trans = 0;
    if(lumi < 0) lumi = 0;

    dst->bucket[freq].reflectance = refl;
    dst->bucket[freq].transmittance = trans;
    dst->bucket[freq].luminosity = lumi;
  }
}

void diluteSpectrumPigment(SpectrumPigment *dst, uint16_t strength16) {
  uint32_t strength = strength16;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    dst->bucket[freq].reflectance = ((65536 - strength) * dst->bucket[freq].reflectance) >> 16;
    dst->bucket[freq].transmittance = (((65536 - strength) * dst->bucket[freq].transmittance) >> 16) + strength;
    dst->bucket[freq].luminosity = ((65536 - strength) * dst->bucket[freq].luminosity) >> 16;
  }
}

void paintSpectrumPigment(SpectrumLayer *spectrumLayer, uint16_t x, uint16_t y,
    const SpectrumPigment *pigment, uint16_t thickness16, uint16_t coverage16) {
  const uint64_t thickness = thickness16;
  const uint64_t coverage = coverage16;

  spectrum *p = spectrumLayer->data + x + y * FRAMEBUFFER_WIDTH;

  uint64_t uncovered = (65536 - coverage);

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    uint32_t reflectance = pigment->bucket[freq].reflectance;
    uint32_t transmittance = pigment->bucket[freq].transmittance;
    uint64_t luminosity = pigment->bucket[freq].luminosity;

    reflectance = (reflectance * thickness) >> 16;
    transmittance = (65536 - thickness) + ((transmittance * thickness) >> 16);
    luminosity = (luminosity * thickness) >> 16;

    uint64_t refl = (
      coverage * (reflectance + ((((transmittance * p->bucket[freq].reflectance) >> 16) * transmittance) >> 16)) +
      uncovered * p->bucket[freq].reflectance
    ) >> 16;
    uint64_t lumi = (
      coverage * (luminosity + ((transmittance * p->bucket[freq].luminosity) >> 16)) +
      uncovered * p->bucket[freq].luminosity
    ) >> 16;

    if(refl > 65535) refl = 65535;
    if(lumi > 65535) lumi = 65535;
    p->bucket[freq].reflectance = refl;
    p->bucket[freq].luminosity = lumi;
  }

  spectrumLayer->dirtyBitmap[(x + y * FRAMEBUFFER_WIDTH) / 8] |= 1 << (x % 8);
}
