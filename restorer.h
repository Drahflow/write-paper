#ifndef H_77426635_2233_44B4_A941_F1CBA229090A
#define H_77426635_2233_44B4_A941_F1CBA229090A

#include "tool.h"

#include "background.h"
#include "spectrum.h"
#include "depth.h"

typedef struct {
  Tool tool;

  Background *background;
  SpectrumLayer *spectrumLayer;
  DepthLayer *depthLayer;
} Restorer;

Restorer *newRestorer(Background *, SpectrumLayer *, DepthLayer *);

#endif
