#ifndef H_61F15C4E_A305_488C_A6C1_707A426DA022
#define H_61F15C4E_A305_488C_A6C1_707A426DA022

#include "tool.h"
#include "depth.h"
#include "spectrum.h"

typedef struct {
  Tool tool;
  
  DepthLayer *depth;
  SpectrumLayer *spectrum;
  uint32_t z;
} Ballpen;

Ballpen *newBallpen(DepthLayer *, SpectrumLayer *);

#endif
