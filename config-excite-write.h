#ifndef H_15FB31B5_05F3_4000_84F1_DF6913769A16
#define H_15FB31B5_05F3_4000_84F1_DF6913769A16

#define WITHPNG
#define FRAMEBUFFER_RGBA
#define FRAMEBUFFER_DEVICE "/dev/fb0"
#define FRAMEBUFFER_WIDTH 2560
#define FRAMEBUFFER_HEIGHT 1600
#define TOUCHSCREEN_DEVICE "/dev/input/event1"
#define DIGITIZER_DEVICE "/dev/input/event2"
#define BACKLIGHT_DEVICE "/sys/class/backlight/pwm-backlight/brightness"

#define EXCITE_WRITE 1

#endif
