#ifndef H_90D019CC_C4FE_4E9B_8B5C_1D778F9621E4
#define H_90D019CC_C4FE_4E9B_8B5C_1D778F9621E4

#include "tool.h"
#include "depth.h"
#include "spectrum.h"
#include "main.h"

#define BRUSH_INTENSITIES 256

typedef struct {
  Tool tool;

  DepthLayer *depth;
  SpectrumLayer *spectrum;

  uint16_t intensities[BRUSH_INTENSITIES];
  uint16_t generation;
  float oldX, oldY;
  uint32_t oldPressure;
} Brush;

Brush *newBrush(DepthLayer *, SpectrumLayer *);

#endif
