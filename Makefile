write-paper2: *.c
	gcc -DCONFIG='"config-excite-write.h"' \
	  -W -Wall -Wextra -Werror -std=c11 -ggdb -O4 $^ -o $@ -lm -lpng

write-paper: old/write-paper.c
	gcc -std=c11 -ggdb -O4 $< -o $@ -lSDL2 -lSDL2_image -lm

write-gn5510: *.c
	arm-linux-gnueabihf-gcc -static -DCONFIG='"config-gn5510.h"' \
	  -W -Wall -Wextra -Werror -std=c11 -ggdb -O4 $^ -o $@ \
	  libpng.a libz.a -lm
