#include "background.h"

#include "main.h"
#include "random.h"
#include "png.h"
#include "srgb.h"
#include "memory.h"

#include <stdio.h>

// TODO: The randomUint32()s in here probably average out, better to seed by x,y
static void restoreBusinessPaper(Background *rawBackground, DepthLayer *depthLayer, SpectrumLayer *spectrumLayer,
    uint16_t x, uint16_t y, uint16_t strength) {
  if(!rawBackground) printf("."); // ignore parameter

  uint32_t new = strength;
  uint32_t old = 65536 - new;

  spectrum *spectrumPixel = spectrumLayer->data + x + y * FRAMEBUFFER_WIDTH;
  depthInformation *depthPixel = depthLayer->data + x + y * FRAMEBUFFER_WIDTH;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    spectrumPixel->bucket[freq].reflectance = (new * 60000 + old * spectrumPixel->bucket[freq].reflectance) >> 16;
    spectrumPixel->bucket[freq].luminosity = (new * 0 + old * spectrumPixel->bucket[freq].luminosity) >> 16;

    depthPixel->depth = (new * (118 + (randomUint32() % 11)) + old * depthPixel->depth) >> 16;
    depthPixel->softness = (new * 64 + old * depthPixel->softness) >> 16;

    uint8_t xRough = randomUint32() % 5;
    depthPixel->xRough = (new * (25 + xRough) + old * depthPixel->xRough) >> 16;
    depthPixel->yRough = (new * (25 - xRough) + old * depthPixel->yRough) >> 16;
  }

  spectrumLayer->dirtyBitmap[(x + y * FRAMEBUFFER_WIDTH) / 8] |= 1 << (x % 8);
}

Background *newBackgroundBusinessPaper(DepthLayer *depthLayer, SpectrumLayer *spectrumLayer) {
  Background *background = assertMalloc(sizeof(Background));
  background->restore = restoreBusinessPaper;

  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        spectrumLayer->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].reflectance = 60000;
        spectrumLayer->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].luminosity = 0;
      }
    }
  }

  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].depth = 118 + (randomUint32() % 11);
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].softness = 64;

      uint8_t xRough = randomUint32() % 5;
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].xRough = 25 + xRough;
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].yRough = 25 - xRough;
    }
  }

  return background;
}

static const uint32_t sRGBred[SPECTRUM] = {
  60632,60632,60632,60632,60632,60632,60632,60632,60632,60632,60632,60632,60632,57264,43790,40421,33684,16842,6736,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,673,1347,2021,2694,3368,3368,3368,3368,3368,
  3368,3368,3368,3368
};
static const uint32_t sRGBgreen[SPECTRUM] = {
  4849,4849,4849,4849,4849,0,0,0,0,0,0,0,0,0,1454,4849,7274,9699,13093,24247,
  29097,30552,33947,36371,37826,38796,38796,37826,36371,36371,36371,34917,31522,28127,26187,21823,13578,4849,969,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0
};
static const uint32_t sRGBblue[SPECTRUM] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,655,1966,3932,6553,13762,20971,24903,28180,32112,34733,38010,41287,43908,44563,45874,46529,
  46529,46529,45874,44563,41942,40631,37354,34733,34078,30801,29490,27524,26214,24247,22937,19660,16383,13107,9830,3276,
  0,0,0,0
};

typedef struct PngBackground {
  Background background;

  rgba data[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT];
} PngBackground;

static void restoreLoadPng(Background *rawBackground, DepthLayer *depthLayer, SpectrumLayer *spectrumLayer,
    uint16_t x, uint16_t y, uint16_t strength) {
  PngBackground *background = (PngBackground*)rawBackground;

  uint32_t new = strength;
  uint32_t old = 65536 - new;

  const uint64_t r = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].r];
  const uint64_t g = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].g];
  const uint64_t b = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].b];

  spectrum *spectrumPixel = spectrumLayer->data + x + y * FRAMEBUFFER_WIDTH;
  depthInformation *depthPixel = depthLayer->data + x + y * FRAMEBUFFER_WIDTH;

  for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
    uint32_t newValue = (
      (r * sRGBred[freq]) +
      (g * sRGBgreen[freq]) +
      (b * sRGBblue[freq])
    ) / 71521.62225;

    spectrumPixel->bucket[freq].reflectance = (new * newValue + old * spectrumPixel->bucket[freq].reflectance) >> 16;
    spectrumPixel->bucket[freq].luminosity = (new * 0 + old * spectrumPixel->bucket[freq].luminosity) >> 16;
  }

  depthPixel->depth = (new * (118 + (randomUint32() % 11)) + old * depthPixel->depth) >> 16;
  depthPixel->softness = (new * 64 + old * depthPixel->softness) >> 16;

  uint8_t xRough = randomUint32() % 5;
  depthPixel->xRough = (new * (25 + xRough) + old * depthPixel->xRough) >> 16;
  depthPixel->yRough = (new * (25 - xRough) + old * depthPixel->yRough) >> 16;

  spectrumLayer->dirtyBitmap[(x + y * FRAMEBUFFER_WIDTH) / 8] |= 1 << (x % 8);
}

Background *newBackgroundLoadPng(DepthLayer *depthLayer, SpectrumLayer *spectrumLayer, const char *filename) {
  PngBackground *background = assertMalloc(sizeof(PngBackground));
  background->background.restore = restoreLoadPng;

  if(!readPng(filename, 0, 0, background->data)) {
    free(background);
    return NULL;
  }

  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      const uint64_t r = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].r];
      const uint64_t g = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].g];
      const uint64_t b = sRGBGamma[background->data[x + y * FRAMEBUFFER_WIDTH].b];

      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        spectrumLayer->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].reflectance = (
          (r * sRGBred[freq]) +
          (g * sRGBgreen[freq]) +
          (b * sRGBblue[freq])
        ) / 71521.62225;
        spectrumLayer->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].luminosity = 0;
      }
    }
  }

  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].depth = 118 + (randomUint32() % 11);
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].softness = 64;

      uint8_t xRough = randomUint32() % 5;
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].xRough = 25 + xRough;
      depthLayer->data[x + y * FRAMEBUFFER_WIDTH].yRough = 25 - xRough;
    }
  }

  return (Background *)background;
}
