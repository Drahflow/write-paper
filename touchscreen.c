#include "touchscreen.h"

#include "main.h"
#include "time.h"
#include "memory.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>

#include <linux/input.h>

Touchscreen *newTouchscreen() {
  Touchscreen *touchscreen = assertMalloc(sizeof(Touchscreen));
  touchscreen->fd = open(TOUCHSCREEN_DEVICE, O_RDONLY | O_NONBLOCK);
  if(touchscreen->fd < 0) {
    fprintf(stderr, "Could not open touchscreen device: %s\n", strerror(errno));
  }

  for(int track = 0; track < TOUCHSCREEN_TRACKS; ++track) {
    touchscreen->oldX[track] = ~0u;
    touchscreen->oldY[track] = ~0u;
    touchscreen->oldWidth[track] = 0;
    touchscreen->width[track] = 0;
  }

  for(int i = 0; i < 4; ++i) {
    touchscreen->fingerTool[i] = NULL;
    touchscreen->stylusTool[i] = NULL;
  }

  return touchscreen;
}

static struct input_event buffer[4096];

static void touchscreenCoordinates(int x, int y, float *screenX, float *screenY) {
  *screenX = x * FRAMEBUFFER_WIDTH / 4096;
  *screenY = y * FRAMEBUFFER_HEIGHT / 4096;
}

static Tool *selectTool(Touchscreen *touchscreen, uint32_t width, uint16_t track) {
  // non-digitizer stylus
  if(width < 25 && track < 4 && touchscreen->stylusTool[track]) {
    return touchscreen->stylusTool[track];
  }

  if(width >= 25 && track < 4 && touchscreen->fingerTool[track]) {
    return touchscreen->fingerTool[track];
  }

  return NULL;
}

void touchscreenInput(Touchscreen *touchscreen) {
  int ret = read(touchscreen->fd, buffer, sizeof(buffer));
  if(ret == -1) return;

  struct input_event *end = (struct input_event *)((char *)buffer + ret);
  for(struct input_event *i = buffer; i != end; ++i) {
    switch(i->type) {
      case EV_ABS:
        switch(i->code) {
          // Touchscreen
          case ABS_MT_POSITION_X: touchscreen->trackX = i->value; break;
          case ABS_MT_POSITION_Y: touchscreen->trackY = i->value; break;
          case ABS_MT_TOUCH_MAJOR: touchscreen->trackWidth = i->value; break;
          case ABS_MT_WIDTH_MAJOR: break; // identical to ABS_MT_TOUCH_MAJOR
          case ABS_MT_TRACKING_ID:
            if(i->value > TOUCHSCREEN_TRACKS) {
              printf("Touchscreen track outside expected range: %d\n", i->value);
            } else {
              touchscreen->x[i->value] = touchscreen->trackX;
              touchscreen->y[i->value] = touchscreen->trackY;
              touchscreen->width[i->value] = touchscreen->trackWidth;
              touchscreen->tracking[i->value] = 1;
            }
            break;
          default:
            printf("Unknown EV_ABS code: %d", i->code);
        }
        break;

      case EV_KEY:
        // printf("Key pressed: %d, %d", i->code, i->value);
        switch(i->code) {
          case BTN_TOUCH:
            // dev->buttons = (dev->buttons & ~BIT_TOUCH) | (i->value * BIT_TOUCH);
            // dev->oldX = -1;
            // dev->oldY = -1;
            break;
          case BTN_STYLUS:
            // dev->buttons = (dev->buttons & ~BIT_STYLUS) | (i->value * BIT_STYLUS);
            break;
        }
        break;

      case EV_SYN:
        if(i->code == SYN_MT_REPORT) {
          // printf("Touchscreen %d,%d %d", touchscreen->x, touchscreen->y, touchscreen->width);
        } else if(i->code == SYN_REPORT) {
          // printf("Touchscreen SYN_REPORT\n");
          uint64_t now = nowMonotone();

          for(int track = 0; track < TOUCHSCREEN_TRACKS; ++track) {
            float drawX, drawY, oldDrawX, oldDrawY;
            touchscreenCoordinates(touchscreen->x[track], touchscreen->y[track], &drawX, &drawY);
            touchscreenCoordinates(touchscreen->oldX[track], touchscreen->oldY[track], &oldDrawX, &oldDrawY);

            if(touchscreen->tracking[track] && !touchscreen->oldTracking[track]) {
              Tool *tool = selectTool(touchscreen, touchscreen->width[track], track);
              if(tool) tool->lower(tool, drawX, drawY);
            }
            if(!touchscreen->tracking[track] && touchscreen->oldTracking[track]) {
              Tool *tool = selectTool(touchscreen, touchscreen->oldWidth[track], track);
              if(tool) tool->raise(tool, oldDrawX, oldDrawY);

              touchscreen->oldX[track] = touchscreen->oldY[track] = ~0u;
            }
            if(!touchscreen->tracking[track]) continue;

            if(touchscreen->oldX[track] != ~0u && touchscreen->oldY[track] != ~0u) {
              Tool *tool = selectTool(touchscreen, touchscreen->width[track], track);
              if(tool) tool->draw(tool, oldDrawX, oldDrawY, drawX, drawY, 20000, now - touchscreen->lastDraw[track]);
            }

            touchscreen->oldX[track] = touchscreen->x[track];
            touchscreen->oldY[track] = touchscreen->y[track];
            touchscreen->lastDraw[track] = now;
            touchscreen->oldTracking[track] = touchscreen->tracking[track];
            touchscreen->oldWidth[track] = touchscreen->width[track];
          }

          for(int track = 0; track < TOUCHSCREEN_TRACKS; ++track) {
            touchscreen->tracking[track] = 0;
          }
        } else {
          printf("Unknown EV_SYN code: %d", i->code);
        }

        break;

      default:
        printf("Unknown i->type: %d", i->type);
        break;
    }
  }
}
