#ifndef H_3C4E1842_EEA5_4F2F_BF74_16933AFE1A6D
#define H_3C4E1842_EEA5_4F2F_BF74_16933AFE1A6D

#include <stdint.h>

extern uint16_t sRGBGamma[];
extern uint8_t sRGBGammaInverse[];

#endif
