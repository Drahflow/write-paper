#define _GNU_SOURCE

#include "main.h"
#include "layer.h"
#include "spectrum.h"
#include "backlight.h"
#include "depth.h"
#include "background.h"
#include "digitizer.h"
#include "touchscreen.h"
#include "ballpen.h"
#include "calibration.h"
#include "png.h"
#include "ui.h"
#include "memory.h"
#include "pigment_data.h"
#include "restorer.h"

#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/epoll.h>
#include <fcntl.h>

#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define EVENT_STREAM_STDIN 1
#define EVENT_STREAM_DIGITIZER 2
#define EVENT_STREAM_TOUCHSCREEN 3

rgba *framebuffer;
rgba *drawingBuffer;

uint8_t initFramebuffer() {
  int fb = open(FRAMEBUFFER_DEVICE, O_RDWR);
  if(fb < 0) {
    fprintf(stderr, "Opening /dev/fb0 failed: %s", strerror(errno));
    return 0;
  }

  framebuffer = mmap(NULL, FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * sizeof(rgba),
      PROT_READ | PROT_WRITE, MAP_SHARED, fb, 0);
  if(!framebuffer) {
    fprintf(stderr, "Mapping framebuffer failed: %s", strerror(errno));
    return 0;
  }

  drawingBuffer = assertMalloc(FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT * sizeof(rgba));

  return 1;
}

int main(int argc, const char **argv) {
  if(!initFramebuffer()) return 1;
  if(!setBacklight(128 << 8)) return 1;

  int epollFd = epoll_create(8);
  if(epollFd < 0) {
    fprintf(stderr, "Could not init epoll: %s", strerror(errno));
    return 1;
  }

  {
    struct epoll_event stdinEvent;
    stdinEvent.events = EPOLLIN;
    stdinEvent.data.u64 = EVENT_STREAM_STDIN;
    if(epoll_ctl(epollFd, EPOLL_CTL_ADD, 0, &stdinEvent) < 0) {
      fprintf(stderr, "Could not start polling for digitizer events: %s", strerror(errno));
      return 1;
    }
  }

  UI *ui = newUI();
  if(!ui) {
    fprintf(stderr, "Failed to initialize UI\n");
    return 1;
  }

  Digitizer *digitizer = newDigitizer();
  digitizer->ui = ui;
  ui->digitizer = digitizer;
  {
    struct epoll_event digitizerEvent;
    digitizerEvent.events = EPOLLIN;
    digitizerEvent.data.u64 = EVENT_STREAM_DIGITIZER;
    if(epoll_ctl(epollFd, EPOLL_CTL_ADD, digitizer->fd, &digitizerEvent) < 0) {
      fprintf(stderr, "Could not start polling for digitizer events: %s", strerror(errno));
      return 1;
    }
  }

  Touchscreen *touchscreen = newTouchscreen();
  {
    struct epoll_event touchscreenEvent;
    touchscreenEvent.events = EPOLLIN;
    touchscreenEvent.data.u64 = EVENT_STREAM_TOUCHSCREEN;
    if(epoll_ctl(epollFd, EPOLL_CTL_ADD, touchscreen->fd, &touchscreenEvent) < 0) {
      fprintf(stderr, "Could not start polling for touchscreen events: %s", strerror(errno));
      return 1;
    }
  }
  ui->touchscreen = touchscreen;

  DepthLayer *depthDrawing = newDepthLayer();
  ui->depthLayer = depthDrawing;
  SpectrumLayer *spectrumDrawing = newSpectrumLayer();
  ui->spectrumLayer = spectrumDrawing;

  Background *background = NULL;

  if(argc == 2) {
    printf("Loading from %s...\n", argv[1]);
    background = newBackgroundLoadPng(depthDrawing, spectrumDrawing, argv[1]);
  }
  if(!background) {
    background = newBackgroundBusinessPaper(depthDrawing, spectrumDrawing);
  }
  ui->backgroundLayer = background;

  // for(uint16_t y = 1000; y < 1100; ++y) {
  //   for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
  //     paintSpectrumPigment(spectrumDrawing, x, y,
  //         &FORS_redlead, x * 65535 / FRAMEBUFFER_WIDTH, 65535);
  //     paintSpectrumPigment(spectrumDrawing, x, y + 100,
  //         &FORS_indigo, x * 65535 / FRAMEBUFFER_WIDTH, 65535);
  //     paintSpectrumPigment(spectrumDrawing, x, y + 200,
  //         &CODE_red, x * 65535 / FRAMEBUFFER_WIDTH, 65535);
  //     paintSpectrumPigment(spectrumDrawing, x, y + 300,
  //         &CODE_yellow, x * 65535 / FRAMEBUFFER_WIDTH, 65535);
  //     paintSpectrumPigment(spectrumDrawing, x, y + 400,
  //         &CODE_green, x * 65535 / FRAMEBUFFER_WIDTH, 65535);
  //   }
  // }

  // drawLinearSpectrum(spectrumDrawing);

  Layer *drawing = (Layer *)spectrumDrawing;
  drawing->renderToFramebuffer(drawing, 0, 0, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT);

  Tool *ballpen = (Tool *)newBallpen(depthDrawing, spectrumDrawing);
  memcpy(&ballpen->pigment, &CODE_pitchblack, sizeof(SpectrumPigment));
  ballpen->width = 1;
  digitizer->tool = ballpen;

  Tool *restorer = (Tool *)newRestorer(background, spectrumDrawing, depthDrawing);
  touchscreen->fingerTool[0] = restorer;

  uint8_t running = 1;
  struct epoll_event eventBuffer[8];
  while(running) {
    int ret = epoll_wait(epollFd, eventBuffer, sizeof(eventBuffer) / sizeof(struct epoll_event), 100);
    if(ret == -1) {
      fprintf(stderr, "Failed to poll for events: %s", strerror(errno));
      running = 0;
    }

    for(int i = 0; i < ret; ++i) {
      switch(eventBuffer[i].data.u64) {
        case EVENT_STREAM_STDIN:
          // Exit on input
          running = 0;
          break;
        case EVENT_STREAM_DIGITIZER: digitizerInput(digitizer); break;
        case EVENT_STREAM_TOUCHSCREEN: touchscreenInput(touchscreen); break;
      }
    }
  }

  if(argc == 2) {
    printf("Saving to %s... ", argv[1]);
    writePng(argv[1], 0, 0, FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT, framebuffer);
    printf("saved.\n");
  }

  return 0;
}
