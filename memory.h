#ifndef H_5EACE953_3372_4AD6_B313_66A01B48B64F
#define H_5EACE953_3372_4AD6_B313_66A01B48B64F

#include <stdlib.h>

void *assertMalloc(size_t size);

#endif
