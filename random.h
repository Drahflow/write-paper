#ifndef H_06B09DFB_B46B_45B0_A19F_447C69FDB174
#define H_06B09DFB_B46B_45B0_A19F_447C69FDB174

#include <stdint.h>

// As per http://www.cse.yorku.ca/~oz/marsaglia-rng.html

#define randomZNew (randomZ=36969*(randomZ&65535)+(randomZ>>16))
#define randomWNew (randomW=18000*(randomW&65535)+(randomW>>16))
#define randomUint32() ((randomZNew<<16)+randomWNew)

extern uint32_t randomZ;
extern uint32_t randomW;

#endif
