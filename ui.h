#ifndef H_5B10A82C_8B6B_4A99_A830_D6E66C8723B2
#define H_5B10A82C_8B6B_4A99_A830_D6E66C8723B2

#include "tool.h"

typedef struct UI {
  void (*btnDown)(struct UI *, float x, float y);
  void (*btnUp)(struct UI *, float x, float y);

  void *digitizer;
  void *touchscreen;
  void *tool;
  void *backgroundLayer;
  void *spectrumLayer;
  void *depthLayer;

  float menuOpenX, menuOpenY;
} UI;

UI *newUI();

#endif
