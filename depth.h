#ifndef H_E1207FC2_15B5_4ACB_8E0E_9E729208B0A4
#define H_E1207FC2_15B5_4ACB_8E0E_9E729208B0A4

#include "layer.h"

typedef struct {
  // 20 depth == 1 pixel x/y
#define DEPTH_SCALE 20
  // 0 = fully depressed, 255 = fully raised
  uint8_t depth;
  // 0 = incompressible, 255 = no resistance
  uint8_t softness;

  // 0 == completely smooth when moving in x, 255 = maximum depth range within
  uint8_t xRough;
  uint8_t yRough;
} depthInformation;

typedef struct {
  Layer layer;

  depthInformation data[];
} DepthLayer;

DepthLayer *newDepthLayer();

#endif
