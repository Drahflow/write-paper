#ifndef H_EA8B0979_B9E1_44A6_ACDC_168E3227FE53
#define H_EA8B0979_B9E1_44A6_ACDC_168E3227FE53

#include "depth.h"
#include "spectrum.h"

typedef struct Background {
  void (*restore)(struct Background *, DepthLayer *, SpectrumLayer *, uint16_t x, uint16_t y, uint16_t strength);
} Background;

Background *newBackgroundBusinessPaper(DepthLayer *, SpectrumLayer *);
Background *newBackgroundLoadPng(DepthLayer *, SpectrumLayer *, const char *filename);

#endif
