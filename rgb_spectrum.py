#!/usr/bin/env python3

import csv

# LCD display filter spectrum
# TODO: This has not been measured on the actual device

# TODO: ... and thus hand-wavy overall factors obtained by comparison
#       with white paper in bright light. :(
r_adjust = 1.028
g_adjust = 0.740
b_adjust = 1.0

data = [
        {"r": 0.9, "g": 0.1, "b": 0.0},
        {"r": 0.9, "g": 0.1, "b": 0.0},
        {"r": 0.9, "g": 0.1, "b": 0.0},
        {"r": 0.9, "g": 0.1, "b": 0.0},
        {"r": 0.9, "g": 0.1, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.9, "g": 0.0, "b": 0.0},
        {"r": 0.85, "g": 0.0, "b": 0.0},
        {"r": 0.65, "g": 0.03, "b": 0.0},
        {"r": 0.6, "g": 0.1, "b": 0.0},
        {"r": 0.5, "g": 0.15, "b": 0.0},
        {"r": 0.25, "g": 0.2, "b": 0.0},
        {"r": 0.1, "g": 0.27, "b": 0.0},
        {"r": 0.0, "g": 0.5, "b": 0.0},
        {"r": 0.0, "g": 0.6, "b": 0.0},
        {"r": 0.0, "g": 0.63, "b": 0.0},
        {"r": 0.0, "g": 0.7, "b": 0.0},
        {"r": 0.0, "g": 0.75, "b": 0.0},
        {"r": 0.0, "g": 0.78, "b": 0.01},
        {"r": 0.0, "g": 0.8, "b": 0.03},
        {"r": 0.0, "g": 0.8, "b": 0.06},
        {"r": 0.0, "g": 0.78, "b": 0.1},
        {"r": 0.0, "g": 0.75, "b": 0.21},
        {"r": 0.0, "g": 0.75, "b": 0.32},
        {"r": 0.0, "g": 0.75, "b": 0.38},
        {"r": 0.0, "g": 0.72, "b": 0.43},
        {"r": 0.0, "g": 0.65, "b": 0.49},
        {"r": 0.0, "g": 0.58, "b": 0.53},
        {"r": 0.0, "g": 0.54, "b": 0.58},
        {"r": 0.0, "g": 0.45, "b": 0.63},
        {"r": 0.0, "g": 0.28, "b": 0.67},
        {"r": 0.0, "g": 0.1, "b": 0.68},
        {"r": 0.0, "g": 0.02, "b": 0.7},
        {"r": 0.0, "g": 0.0, "b": 0.71},
        {"r": 0.0, "g": 0.0, "b": 0.71},
        {"r": 0.0, "g": 0.0, "b": 0.71},
        {"r": 0.0, "g": 0.0, "b": 0.7},
        {"r": 0.0, "g": 0.0, "b": 0.68},
        {"r": 0.0, "g": 0.0, "b": 0.64},
        {"r": 0.0, "g": 0.0, "b": 0.62},
        {"r": 0.0, "g": 0.0, "b": 0.57},
        {"r": 0.0, "g": 0.0, "b": 0.53},
        {"r": 0.0, "g": 0.0, "b": 0.52},
        {"r": 0.0, "g": 0.0, "b": 0.47},
        {"r": 0.0, "g": 0.0, "b": 0.45},
        {"r": 0.01, "g": 0.0, "b": 0.42},
        {"r": 0.02, "g": 0.0, "b": 0.40},
        {"r": 0.03, "g": 0.0, "b": 0.37},
        {"r": 0.04, "g": 0.0, "b": 0.35},
        {"r": 0.05, "g": 0.0, "b": 0.3},
        {"r": 0.05, "g": 0.0, "b": 0.25},
        {"r": 0.05, "g": 0.0, "b": 0.2},
        {"r": 0.05, "g": 0.0, "b": 0.15},
        {"r": 0.05, "g": 0.0, "b": 0.05},
        {"r": 0.05, "g": 0.0, "b": 0.0},
        {"r": 0.05, "g": 0.0, "b": 0.0},
        {"r": 0.05, "g": 0.0, "b": 0.0},
        {"r": 0.05, "g": 0.0, "b": 0.0},
]

S_CONE_SENSITIVITY = [
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 1, 2, 5, 11,
  24, 48, 78, 128, 270, 530, 825, 1270, 1913, 2805,
  3985, 5825, 9285, 13884, 19026, 22303, 25580, 33842, 42359, 48382,
  51556, 56375, 62611, 64978, 64962, 64946, 59215, 52595, 44201, 39913,
  35626, 24992, 20131, 15270, 8024, 5868, 3712, 1561, 1093, 625,
  312, 156, 78, 0
]

M_CONE_SENSITIVITY = [
  0, 2, 4, 9, 20, 42, 90, 190, 402, 857,
  1428, 2487, 4839, 7644, 11845, 17358, 24433, 32282, 40168, 48514,
  55269, 60144, 62689, 64040, 65286, 63494, 61320, 58034, 53516, 47129,
  40338, 33788, 25716, 19898, 17567, 16508, 15450, 13460, 11527, 9472,
  7619, 6435, 5704, 4972, 4609, 4245, 3396, 2585, 1937, 1678,
  1419, 952, 764, 576, 308, 228, 148, 62, 43, 24,
  12, 6, 3, 0
]

L_CONE_SENSITIVITY = [
  11, 35, 73, 154, 327, 676, 1412, 2807, 5292, 9498,
  13977, 19452, 28856, 36321, 43793, 50796, 56358, 60795, 63078, 65031,
  65354, 64318, 63289, 61615, 58984, 54112, 50503, 46183, 41192, 35159,
  29076, 23573, 17100, 12553, 10744, 9964, 9184, 7785, 6519, 5287,
  4241, 3626, 3267, 2945, 2791, 2638, 2235, 1847, 1502, 1355,
  1208, 877, 724, 571, 316, 237, 157, 68, 48, 27,
  13, 6, 3, 0
]

s_cone = {"r": 0, "g": 0, "b": 0}
m_cone = {"r": 0, "g": 0, "b": 0}
l_cone = {"r": 0, "g": 0, "b": 0}

for i in range(64):
    frequency = 400 + i * 6.25 + (6.25 / 2)
    wavelength = 300000 / frequency

    s_cone["r"] += r_adjust * data[i]["r"] * S_CONE_SENSITIVITY[i]
    s_cone["g"] += g_adjust * data[i]["g"] * S_CONE_SENSITIVITY[i]
    s_cone["b"] += b_adjust * data[i]["b"] * S_CONE_SENSITIVITY[i]
    m_cone["r"] += r_adjust * data[i]["r"] * M_CONE_SENSITIVITY[i]
    m_cone["g"] += g_adjust * data[i]["g"] * M_CONE_SENSITIVITY[i]
    m_cone["b"] += b_adjust * data[i]["b"] * M_CONE_SENSITIVITY[i]
    l_cone["r"] += r_adjust * data[i]["r"] * L_CONE_SENSITIVITY[i]
    l_cone["g"] += g_adjust * data[i]["g"] * L_CONE_SENSITIVITY[i]
    l_cone["b"] += b_adjust * data[i]["b"] * L_CONE_SENSITIVITY[i]

    print(frequency, wavelength, data[i]["r"], data[i]["g"], data[i]["b"])

print("S-cone", s_cone)
print("M-cone", m_cone)
print("L-cone", l_cone)

# lest anyone gets confused, afaik my x/y have nothing to do with CIE
x = m_cone["g"] / l_cone["g"]
m_minus_xl = {
        "r": m_cone["r"] - x * l_cone["r"],
        "g": m_cone["g"] - x * l_cone["g"],
        "b": m_cone["b"] - x * l_cone["b"],
}

print("M - x*L", m_minus_xl)

y = s_cone["g"] / l_cone["g"]
s_minus_yl = {
        "r": s_cone["r"] - y * l_cone["r"],
        "g": s_cone["g"] - y * l_cone["g"],
        "b": s_cone["b"] - y * l_cone["b"],
}

print("S - y*L", s_minus_yl)

z = m_minus_xl["b"] / s_minus_yl["b"]
mxl_minus_syl = {
        "r": m_minus_xl["r"] - z * s_minus_yl["r"],
        "g": m_minus_xl["g"] - z * s_minus_yl["g"],
        "b": m_minus_xl["b"] - z * s_minus_yl["b"],
}

print("(M - x*L) - z * (S - y*L)", mxl_minus_syl)

red = {
        "s": (-z) / mxl_minus_syl["r"],
        "m": 1 / mxl_minus_syl["r"],
        "l": ((-x) - (z * -y)) / mxl_minus_syl["r"],
}

z2 = m_minus_xl["r"] / s_minus_yl["r"]
mxl_minus_syl2 = {
        "r": m_minus_xl["r"] - z2 * s_minus_yl["r"],
        "g": m_minus_xl["g"] - z2 * s_minus_yl["g"],
        "b": m_minus_xl["b"] - z2 * s_minus_yl["b"],
}

print("(M - x*L) - z2 * (S - y*L)", mxl_minus_syl2)

blue = {
        "s": (-z2) / mxl_minus_syl2["b"],
        "m": 1 / mxl_minus_syl2["b"],
        "l": (-x - z2 * (-y)) / mxl_minus_syl2["b"],
}

x2 = m_cone["b"] / l_cone["b"]
m_minus_xl2 = {
        "r": m_cone["r"] - x2 * l_cone["r"],
        "g": m_cone["g"] - x2 * l_cone["g"],
        "b": m_cone["b"] - x2 * l_cone["b"],
}

print("M - x2*L", m_minus_xl2)

y2 = s_cone["b"] / l_cone["b"]
s_minus_yl2 = {
        "r": s_cone["r"] - y2 * l_cone["r"],
        "g": s_cone["g"] - y2 * l_cone["g"],
        "b": s_cone["b"] - y2 * l_cone["b"],
}

print("S - y2*L", s_minus_yl2)

z3 = m_minus_xl2["r"] / s_minus_yl2["r"]
mxl_minus_syl3 = {
        "r": m_minus_xl2["r"] - z3 * s_minus_yl2["r"],
        "g": m_minus_xl2["g"] - z3 * s_minus_yl2["g"],
        "b": m_minus_xl2["b"] - z3 * s_minus_yl2["b"],
}

print("(M - x2*L) - z3 * (S - y2*L)", mxl_minus_syl3)

green = {
        "s": (-z3) / mxl_minus_syl3["g"],
        "m": 1 / mxl_minus_syl3["g"],
        "l": (-x2 - z3 * (-y2)) / mxl_minus_syl3["g"],
}

print("Red", red)
print("Green", green)
print("Blue", blue)

# Test drive for (s=0, m=1, l=0)
r = red["l"]
g = green["l"]
b = blue["l"]

print(r, g, b)

s = s_cone["r"] * r + s_cone["g"] * g + s_cone["b"] * b
m = m_cone["r"] * r + m_cone["g"] * g + m_cone["b"] * b
l = l_cone["r"] * r + l_cone["g"] * g + l_cone["b"] * b

print({"s": s, "m": m, "l": l})

scale = 100000 * 65535;

print("== for spectrum.c ==")
print("      int32_t r = (", int(red["s"] * scale), " * s + ", int(red["m"] * scale), " * m + ", int(red["l"] * scale), " * l) >> (16 + 8);")
print("      int32_t g = (", int(green["s"] * scale), " * s + ", int(green["m"] * scale), " * m + ", int(green["l"] * scale), " * l) >> (16 + 8);")
print("      int32_t b = (", int(blue["s"] * scale), " * s + ", int(blue["m"] * scale), " * m + ", int(blue["l"] * scale), " * l) >> (16 + 8);")
print()

print("== for background.c, sRGB loading ==")
print("static const uint32_t sRGBred[SPECTRUM] = {\n  ", end="")
for i in range(64):
    print(str(int(r_adjust * data[i]["r"] * 65535)) + ("," if i != 63 else ""), end = "\n  " if i % 20 == 19 else "")
print("\n};")

print("static const uint32_t sRGBgreen[SPECTRUM] = {\n  ", end="")
for i in range(64):
    print(str(int(g_adjust * data[i]["g"] * 65535)) + ("," if i != 63 else ""), end = "\n  " if i % 20 == 19 else "")
print("\n};")

print("static const uint32_t sRGBblue[SPECTRUM] = {\n  ", end="")
for i in range(64):
    print(str(int(b_adjust * data[i]["b"] * 65535)) + ("," if i != 63 else ""), end = "\n  " if i % 20 == 19 else "")
print("\n};")

maximum = 0
for i in range(64):
    maximum = max(maximum,
        int(r_adjust * data[i]["r"] * 65535) * 65535 +
        int(g_adjust * data[i]["g"] * 65535) * 65535 +
        int(b_adjust * data[i]["b"] * 65535) * 65535
    )

print("Divisor in spectrum builtup: ", maximum / 60000);
