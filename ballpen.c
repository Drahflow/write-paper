#define _GNU_SOURCE

#include "ballpen.h"

#include "memory.h"
#include "main.h"
#include "random.h"

#include <stdio.h>
#include <math.h>

static void lower(Tool *rawTool, float x, float y) {
  Ballpen *ballpen = (Ballpen *)rawTool;

  float width = 2 * ballpen->tool.width;

  // Start at paper surface
  ballpen->z = (ballpen->depth->data[(uint16_t)x + (uint16_t)y * FRAMEBUFFER_WIDTH].depth + DEPTH_SCALE * width) * (1ull << 24);
}

static void raise(Tool *rawTool, float x, float y) {
  if(rawTool && x == y && x == -7) printf("."); // ignore parameters
}

static void draw(Tool *rawTool, float sx, float sy, float ex, float ey, uint32_t pressure, uint64_t dt) {
  Ballpen *ballpen = (Ballpen *)rawTool;

  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);
  float width = 2 * ballpen->tool.width;

  int32_t dz = 0;
  uint32_t ddz = pressure * dt / dist;
  ddz = ddz >> 4;

  uint32_t vx = fabs(dx / dist) * 65535;
  uint32_t vy = fabs(dy / dist) * 65535;

  uint32_t opacity = 2048 + pressure / 8;

  for(float f = 0; f < 1; f += 0.5 / dist) {
    if(pressure < randomUint32() % 20000) {
      // Ball did not turn
      continue;
    }

    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    dz += ddz;
    if(dz > 0 && ballpen->z < (uint32_t)dz) {
      ballpen->z = 0;
      dz = 0;
    } else {
      ballpen->z -= dz;
    }

    float beginX = cx - width;
    float beginY = cy - width;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 1;
    float endY = cy + width + 1;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    uint32_t pushedZ = ballpen->z;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        float pdx = xx - cx;
        float pdy = yy - cy;
        float pdist = sqrtf(pdx * pdx + pdy * pdy);
        if(pdist > width + 1) continue;

        depthInformation *d = ballpen->depth->data + xx + yy * FRAMEBUFFER_WIDTH;
        uint32_t paperZ = d->depth << 24;

        // Adjust for ball shape
        uint32_t ballShape = DEPTH_SCALE * sqrtf(width * width - pdist * pdist) * (1 << 24);
        uint32_t localZ = ballpen->z - ballShape;

        // printf("z: %u, lz: %u, pz: %u, bs: %u\n", z, localZ, paperZ, ballShape);

        if(localZ > paperZ) continue;
        if(paperZ + ballShape > pushedZ) pushedZ = paperZ + ballShape;

        d->depth = localZ >> 24;

        uint32_t intensity = (vx * d->xRough + vy * d->yRough) >> 3;
        if(pdist > width) {
          intensity = intensity * (pdist - width);
        }

        paintSpectrumPigment(
            ballpen->spectrum, xx, yy,
            &ballpen->tool.pigment,
            65535,
            (intensity * opacity) >> 16
        );
      }
    }

    // printf("z: %u, dz: %u, ddz: %u, pz: %u, vx: %u, vy: %u\n", z, dz, ddz, pushedZ, vx, vy);

    if(ballpen->z < pushedZ) {
      ballpen->z = pushedZ;
      if(dz > 0) dz = -dz / 2;
    }
  }

  for(float f = 0; f < 1; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 1;
    float endY = cy + width + 1;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    ((Layer *)ballpen->spectrum)->renderToFramebuffer((Layer *)ballpen->spectrum, beginX, beginY, endX - beginX, endY - beginY);
  }
}

Ballpen *newBallpen(DepthLayer *depth, SpectrumLayer *spectrum) {
  Ballpen *ballpen = assertMalloc(sizeof(Ballpen));
  ballpen->depth = depth;
  ballpen->spectrum = spectrum;

  ballpen->tool.lower = lower;
  ballpen->tool.raise = raise;
  ballpen->tool.draw = draw;

  return ballpen;
}
