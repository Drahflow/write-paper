#ifndef H_AB9A48B2_452A_448A_9EAD_F227AE89DCB5
#define H_AB9A48B2_452A_448A_9EAD_F227AE89DCB5

#include "tool.h"
#include "depth.h"
#include "spectrum.h"

typedef struct {
  Tool tool;
  
  DepthLayer *depth;
  SpectrumLayer *spectrum;
  uint32_t z;
  int32_t dz;
  uint16_t generation;
} ColoredPencil;

ColoredPencil *newColoredPencil(DepthLayer *, SpectrumLayer *);

#endif
