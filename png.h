#ifndef H_BE01B72C_78E5_42E4_9677_C1A3B7743C88
#define H_BE01B72C_78E5_42E4_9677_C1A3B7743C88

#include <stdint.h>

#include "main.h"

// to and from the framebuffer
int readPng(const char *filename, uint32_t x, uint32_t y, rgba *target);
int writePng(const char *filename, uint32_t x, uint32_t y, uint32_t w, uint32_t h, rgba *source);

#endif
