#include "png.h"

#include "memory.h"
#include "main.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <png.h>

#ifdef WITHPNG
int writePng(const char *filename, uint32_t x, uint32_t y, uint32_t w, uint32_t h, rgba *source) {
  // Set alpha channel to opaque
  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      source[x + y * FRAMEBUFFER_WIDTH].a = 255;
    }
  }

  FILE *f = fopen(filename, "wb");
  if(!f) {
    fprintf(stderr, "Could not open %s for writing: %s\n", filename, strerror(errno));
    return 0;
  }

  png_structp png_ptr = png_create_write_struct(
      PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if(!png_ptr) {
    fprintf(stderr, "Could not initialize PNG writer.\n");
    fclose(f);
    return 0;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if(!info_ptr) {
    fprintf(stderr, "Could not initialize PNG info struct.\n");
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    fclose(f);
    return 0;
  }

  if(setjmp(png_jmpbuf(png_ptr))) {
    fprintf(stderr, "PNG writer encountered an error.\n");
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(f);
    return 0;
  }

  png_init_io(png_ptr, f);
  png_set_IHDR(png_ptr, info_ptr, w, h,
      8, // bits per channel
      PNG_COLOR_TYPE_RGBA,
      PNG_INTERLACE_ADAM7,
      PNG_COMPRESSION_TYPE_DEFAULT,
      PNG_FILTER_TYPE_DEFAULT
  );
  png_set_sRGB(png_ptr, info_ptr, PNG_sRGB_INTENT_PERCEPTUAL);
  // 217x135mm at 2560x1600
  png_set_pHYs(png_ptr, info_ptr, 11797, 11852, PNG_RESOLUTION_METER);

  rgba **row_ptrs = assertMalloc(sizeof(rgba *) * h);
  for(uint32_t yy = y; yy < y + h; ++yy) {
    row_ptrs[yy - y] = source + x + yy * FRAMEBUFFER_WIDTH;
  }
  // png_set_filler(png_ptr, 0, PNG_FILLER_AFTER);

  png_write_info(png_ptr, info_ptr);
  png_write_image(png_ptr, (png_bytepp)row_ptrs);
  png_write_end(png_ptr, info_ptr);

  png_destroy_write_struct(&png_ptr, &info_ptr);
  free(row_ptrs);
  fclose(f);

  return 1;
}

int readPng(const char *filename, uint32_t x, uint32_t y, rgba *target) {
  FILE *f = fopen(filename, "rb");
  if(!f) {
    fprintf(stderr, "Can't open %s: %s\n", filename, strerror(errno));
    return 0;
  }

  png_structp png_ptr = png_create_read_struct(
      PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if(!png_ptr) {
    fprintf(stderr, "Could not initialize PNG reader.\n");
    fclose(f);
    return 0;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if(!info_ptr) {
    fprintf(stderr, "Could not initialize PNG info struct.\n");
    png_destroy_read_struct(&png_ptr, (png_infopp)NULL, NULL);
    fclose(f);
    return 0;
  }

  if(setjmp(png_jmpbuf(png_ptr))) {
    fprintf(stderr, "PNG reader encountered an error.\n");
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(f);
    return 0;
  }

  png_init_io(png_ptr, f);
  png_read_info(png_ptr, info_ptr);

  png_uint_32 width, height;
  int bit_depth, color_type, interlace_type;

  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
    &interlace_type, NULL, NULL);

  if(x + width > FRAMEBUFFER_WIDTH || y + height > FRAMEBUFFER_HEIGHT) {
    fprintf(stderr, "PNG being loaded would not fit.\n");
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(f);
    return 0;
  }

  png_set_filler(png_ptr, 0, PNG_FILLER_AFTER);

  // png_set_gamma(png_ptr, PNG_DEFAULT_sRGB, PNG_DEFAULT_sRGB);
  png_read_update_info(png_ptr, info_ptr);

  // Regarding the sad fact that the image might not fit: YOLO
  rgba **row_ptrs = assertMalloc(sizeof(rgba *) * height);
  for(uint32_t yy = y; yy < y + height; ++yy) {
    row_ptrs[yy - y] = target + x + yy * FRAMEBUFFER_WIDTH;
  }

  png_read_image(png_ptr, (png_bytepp)row_ptrs);
  png_read_end(png_ptr, info_ptr);

  png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
  free(row_ptrs);
  fclose(f);

  return 1;
}

#else

int writePng(const char *, uint32_t, uint32_t, uint32_t, uint32_t, rgba *) { return 0; }
int readPng(const char *, uint32_t, uint32_t, rgba *) { return 0; }

#endif
