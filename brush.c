#include "brush.h"

#include "random.h"
#include "memory.h"

#include <math.h>
#include <stdio.h>
#include <strings.h>

#define GENERATION_OVERLAP_PROTECTION 2
#define BRUSH_WIDTH_SCALE 4200.0

static void lower(Tool *rawTool, float x, float y) {
  if(!rawTool && x + y < 0) printf("."); // ignore parameters

  Brush *brush = (Brush *)rawTool;

  for(int i = 0; i < BRUSH_INTENSITIES; ++i) {
    brush->intensities[i] = 35536 + randomUint32() % 30000;
  }
  for(int i = 2; i < BRUSH_INTENSITIES; ++i) {
    brush->intensities[i] = ((uint32_t)brush->intensities[i - 2] + brush->intensities[i - 1] + brush->intensities[i]) / 3;
  }
  brush->generation = GENERATION_OVERLAP_PROTECTION;

  brush->oldPressure = 0;
  brush->oldX = x;
  brush->oldY = y;
}

static void raise(Tool *rawTool, float x, float y) {
  if(!rawTool && x + y < 0) printf("."); // ignore parameters

  bzero(paintGeneration, sizeof(paintGeneration));
}

static void draw(Tool *rawTool, float sx, float sy, float ex, float ey, uint32_t pressure, uint64_t dt) {
  if(!rawTool && dt > 0) printf("."); // ignore paremeters

  Brush *brush = (Brush *)rawTool;

  sx = brush->oldX;
  sy = brush->oldY;
  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);
  if(dist < 1) return;

  ++brush->generation;

  for(float f = 0.0; f < 1.0; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;
    float width = ((brush->oldPressure * (1 - f)) + (pressure * f)) / BRUSH_WIDTH_SCALE * brush->tool.width;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 2;
    float endY = cy + width + 2;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        if(brush->generation - paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] < GENERATION_OVERLAP_PROTECTION) {
          paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] = brush->generation;
          continue;
        }

        float pdx = xx - cx;
        float pdy = yy - cy;
        float orthogonalDist = (pdx * dy - pdy * dx) / dist;
        if(fabs(orthogonalDist) > width + 1) continue;
        if(sqrtf(pdx * pdx + pdy * pdy) > width + 1) continue;
        if(pdx * dx + pdy * dy > 0) continue;

        uint32_t borderIntensity = fabs(orthogonalDist) <= width? 65535: 65535 * (1 + width - fabs(orthogonalDist));

        orthogonalDist /= (1 + width / 16.0); // brush pattern contracts somewhat on lower pressure

        uint32_t intensityIndex = BRUSH_INTENSITIES / 2 + orthogonalDist;
        uint32_t weightNext = (BRUSH_INTENSITIES / 2 + orthogonalDist - intensityIndex) * 65535;

        uint32_t intensity = (((
          (65536 - weightNext) * brush->intensities[intensityIndex] +
          weightNext * brush->intensities[intensityIndex + 1]
        ) >> 16) * borderIntensity) >> 16;

        paintSpectrumPigment(
            brush->spectrum, xx, yy,
            &brush->tool.pigment,
            intensity,
            65535
        );

        paintGeneration[xx + yy * FRAMEBUFFER_WIDTH] = brush->generation;
      }
    }
  }

  for(float f = 0; f < 1; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;
    float width = ((brush->oldPressure * (1 - f)) + (pressure * f)) / BRUSH_WIDTH_SCALE * brush->tool.width;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 2;
    float endY = cy + width + 2;
    if(endX >= FRAMEBUFFER_WIDTH) endX = FRAMEBUFFER_WIDTH;
    if(endY >= FRAMEBUFFER_HEIGHT) endY = FRAMEBUFFER_HEIGHT;

    ((Layer *)brush->spectrum)->renderToFramebuffer((Layer *)brush->spectrum, beginX, beginY, endX - beginX, endY - beginY);
  }

  brush->oldPressure = pressure;
  brush->oldX = ex;
  brush->oldY = ey;
}

Brush *newBrush(DepthLayer *depthLayer, SpectrumLayer *spectrumLayer) {
  Brush *brush = assertMalloc(sizeof(Brush));
  brush->depth = depthLayer;
  brush->spectrum = spectrumLayer;

  brush->tool.lower = lower;
  brush->tool.raise = raise;
  brush->tool.draw = draw;

  return brush;
}
