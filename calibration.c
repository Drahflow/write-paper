#include "spectrum.h"
#include "main.h"

#include <math.h>

void drawLinearSpectrum(SpectrumLayer *drawing) {
  for(uint32_t y = 0; y < FRAMEBUFFER_HEIGHT; ++y) {
    for(uint32_t x = 0; x < FRAMEBUFFER_WIDTH; ++x) {
      float realFreq = x * 64.0 / FRAMEBUFFER_WIDTH;

      for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
        drawing->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].reflectance = 0;
        drawing->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].luminosity = 0;
      }

      if(y >= 100 && y <= 200) {
        for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
          drawing->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].reflectance =
            65535 * expf(-(freq - realFreq) * (freq - realFreq) / 6);
        }
      } else if(y >= 300 && y < 400) {
        for(uint32_t freq = 0; freq < SPECTRUM; ++freq) {
          drawing->data[x + y * FRAMEBUFFER_WIDTH].bucket[freq].luminosity =
            16384 * expf(-(freq - realFreq) * (freq - realFreq) / 6);
        }
      }
    }
  }
}
