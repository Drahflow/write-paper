#include "tool.h"
#include "main.h"

#include <stdio.h>

uint16_t paintGeneration[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT] = {0};

void ignoreTool(Tool *rawTool, float x, float y) {
  if(!rawTool && x == y) printf("."); // ignore parameters
}
