#ifndef H_CD1EEA3F_8462_47A8_AF73_A83739CF8271
#define H_CD1EEA3F_8462_47A8_AF73_A83739CF8271

#include <stdint.h>

#include "spectrum.h"

typedef struct Tool {
  void (*lower)(struct Tool *, float x, float y);
  void (*raise)(struct Tool *, float x, float y);
  // pressure max: 65535
  void (*draw)(struct Tool *, float oldX, float oldY, float x, float y, uint32_t pressure, uint64_t dt);

  SpectrumPigment pigment;
  float width; // normalized to 1
} Tool;

void ignoreTool(Tool *rawTool, float x, float y);

#endif
