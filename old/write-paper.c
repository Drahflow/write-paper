#define _GNU_SOURCE

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <math.h>

SDL_Window *window;
SDL_Surface *surface;

SDL_Surface *backgroundImage;

struct rgba {
  uint8_t r, g, b, a;
};

struct rgb {
  uint8_t r, g, b;
};

struct rgbaf {
  float r, g, b, a;
};

#define BIT_TOUCH 1
#define BIT_STYLUS 2

struct input {
  int fd;
  int buttons;
  int pressure;
  int oldPressure;
  int width;
  int x, y;
  int oldX, oldY;
  int stylusTime;
} touchscreen, digitizer;

struct rgba nowhere;

static int updates;
static SDL_Rect updateRects[65536];

struct rgbaf COLORS[] = {
  { .r = 0.0, .g = 0.0, .b = 0.0, .a = 0.0 },
  { .r = 0.0, .g = 0.0, .b = 0.7, .a = 0.0 },
  { .r = 0.9, .g = 0.0, .b = 0.0, .a = 0.0 },
  { .r = 0.0, .g = 0.7, .b = 0.0, .a = 0.0 },
  { .r = 1.0, .g = 1.0, .b = 1.0, .a = 0.0 },
};
int COLORS_COUNT = sizeof(COLORS) / sizeof(COLORS[0]);
int colorIdx = 0;

struct rgba *pixel(int x, int y) {
  if(x < 0 || y < 0 || x >= surface->w || y >= surface->h) return &nowhere;

  return (struct rgba *)((char *)surface->pixels + x * sizeof(struct rgba) + y * surface->pitch);
}

void background(struct rgba *p, int x, int y) {
  if(backgroundImage && x > 0 && x < backgroundImage->w && y > 0 && y < backgroundImage->h) {
    if(backgroundImage->format->BitsPerPixel == 24) {
      struct rgb *bg = (struct rgb *)((char *)backgroundImage->pixels + x * sizeof(struct rgb) + y * backgroundImage->pitch);
      p->r = bg->r;
      p->g = bg->g;
      p->b = bg->b;
    } else {
      struct rgba *bg = (struct rgba *)((char *)backgroundImage->pixels + x * sizeof(struct rgba) + y * backgroundImage->pitch);
      p->r = bg->b;
      p->g = bg->g;
      p->b = bg->r;
    }
  } else {
    p->r = (y % 50 && x % 50)? 255: 200;
    p->g = (y % 50 && x % 50)? 252: 190;
    p->b = (y % 50 && x % 50)? 248: 220;
  }
}

void digitizerCoordinates(int x, int y, float *screenX, float *screenY) {
  *screenX = (x - 91.0) * 2560 / (21658 + 91);
  *screenY = (y + 10.0) * 1600 / (13536);

  // float yFixup = 0;
  // if(*screenY > 50 && *screenY < 80) {
  //   yFixup = (*screenY - 50) / 30.0;
  // } else if(*screenY >= 70 && *screenY < 210) {
  //   yFixup = 1;
  // } else if(*screenY >= 200 && *screenY < 230) {
  //   yFixup = (230 - *screenY) / 30.0;
  // }
  // *screenY = *screenY + 11 * yFixup;
}

void touchscreenCoordinates(int x, int y, float *screenX, float *screenY) {
  *screenX = x * 2560 / 4096;
  *screenY = y * 1600 / 4096;
}

void drawLine(float sx, float sy, float ex, float ey, float pressure, float width,
    float r, float g, float b, int structure) {
  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);

  r = 1.0 - r;
  g = 1.0 - g;
  b = 1.0 - b;

  for(float f = 0; f < 1; f += 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 1;
    float endY = cy + width + 1;
    if(endX >= surface->w) endX = surface->w;
    if(endY >= surface->h) endY = surface->h;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        struct rgba *p = pixel(xx, yy);

        float pdx = xx - cx;
        float pdy = yy - cy;
        float pdist = sqrtf(pdx * pdx + pdy * pdy) / width;
        float intensity = pdist > 1? 0: cos(pdist * M_PI_2);

        intensity *= pressure * 10.0;
        if(structure) {
          intensity *= (rand() % 1000) > 200? 0.05: 2.0;
        }

        if(!r && !g && !b) {
          // special-case white
          p->r = p->r + intensity < 255? p->r + intensity: 255;
          p->g = p->g + intensity < 255? p->g + intensity: 255;
          p->b = p->b + intensity < 255? p->b + intensity: 255;
        } else {
          float dr = r * intensity;
          float dg = g * intensity;
          float db = b * intensity;

          p->r = p->r < dr? 0: p->r - dr;
          p->g = p->g < dg? 0: p->g - dg;
          p->b = p->b < db? 0: p->b - db;
        }
      }
    }

    SDL_Rect *upd = updateRects + updates++;
    upd->x = beginX;
    upd->y = beginY;
    upd->w = endX - beginX;
    upd->h = endY - beginY;
  }
}

void deleteLine(float sx, float sy, float ex, float ey, float width) {
  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);

  for(float f = 0; f < 1; f += width * 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 1;
    float endY = cy + width + 1;
    if(endX >= surface->w) endX = surface->w;
    if(endY >= surface->h) endY = surface->h;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        struct rgba *p = pixel(xx, yy);

        float pdx = xx - cx;
        float pdy = yy - cy;
        float pdist = sqrtf(pdx * pdx + pdy * pdy) / width;
        float intensity = pdist > 1? 0: cos(pdist * M_PI_2);

        struct rgba bg;
        background(&bg, xx, yy);

        p->r = p->r * (1.0 - intensity) + bg.r * intensity;
        p->g = p->g * (1.0 - intensity) + bg.g * intensity;
        p->b = p->b * (1.0 - intensity) + bg.b * intensity;
      }
    }

    SDL_Rect *upd = updateRects + updates++;
    upd->x = beginX;
    upd->y = beginY;
    upd->w = endX - beginX;
    upd->h = endY - beginY;
  }
}

void blurLine(float sx, float sy, float ex, float ey, float width) {
  float dx = ex - sx;
  float dy = ey - sy;
  float dist = sqrtf(dx * dx + dy * dy);

  for(float f = 0; f < 1; f += width * 0.5 / dist) {
    float cx = sx * (1 - f) + ex * f;
    float cy = sy * (1 - f) + ey * f;

    float beginX = cx - width - 1;
    float beginY = cy - width - 1;
    if(beginX < 0) beginX = 0;
    if(beginY < 0) beginY = 0;
    float endX = cx + width + 1;
    float endY = cy + width + 1;
    if(endX >= surface->w) endX = surface->w;
    if(endY >= surface->h) endY = surface->h;

    float sumR = 0;
    float sumG = 0;
    float sumB = 0;
    float sumWeight = 0;

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        struct rgba *p = pixel(xx, yy);

        float pdx = xx - cx;
        float pdy = yy - cy;
        float pdist = sqrtf(pdx * pdx + pdy * pdy) / width;
        float intensity = pdist > 1? 0: cos(pdist * M_PI_2);

        sumR += p->r * intensity;
        sumG += p->g * intensity;
        sumB += p->b * intensity;
        sumWeight += intensity;
      }
    }

    struct rgba avg = {
      .r = sumR / sumWeight,
      .g = sumG / sumWeight,
      .b = sumB / sumWeight
    };

    for(int yy = beginY; yy < endY; ++yy) {
      for(int xx = beginX; xx < endX; ++xx) {
        struct rgba *p = pixel(xx, yy);

        float pdx = xx - cx;
        float pdy = yy - cy;
        float pdist = sqrtf(pdx * pdx + pdy * pdy) / width;
        float intensity = pdist > 1? 0: cos(pdist * M_PI_2);
        intensity /= 5;

        p->r = p->r * (1.0 - intensity) + avg.r * intensity;
        p->g = p->g * (1.0 - intensity) + avg.g * intensity;
        p->b = p->b * (1.0 - intensity) + avg.b * intensity;
      }
    }

    SDL_Rect *upd = updateRects + updates++;
    upd->x = beginX;
    upd->y = beginY;
    upd->w = endX - beginX;
    upd->h = endY - beginY;
  }
}

// Drop all pending events; used to avoid jumps after a save
void dropEvents(struct input *dev) {
  static const uint32_t bufferSize = sizeof(struct input_event) * 4096;
  static struct input_event *buffer = NULL;
  if(!buffer) buffer = malloc(bufferSize);

  while(read(dev->fd, buffer, bufferSize) > 0);
}

void process(struct input *dev) {
  static const uint32_t bufferSize = sizeof(struct input_event) * 4096;
  static struct input_event *buffer = NULL;
  if(!buffer) buffer = malloc(bufferSize);

  int ret = read(dev->fd, buffer, bufferSize);
  if(ret == -1) return;

  struct input_event *end = (struct input_event *)((char *)buffer + ret);
  for(struct input_event *i = buffer; i != end; ++i) {
    if(dev->buttons & BTN_STYLUS) {
      ++dev->stylusTime;
    }

    switch(i->type) {
      case EV_ABS:
        switch(i->code) {
          // Digitizer
          case ABS_X: dev->x = i->value; break;
          case ABS_Y: dev->y = i->value; break;
          case ABS_PRESSURE: dev->pressure = i->value; break;
          // Touchscreen
          case ABS_MT_POSITION_X: dev->x = i->value; break;
          case ABS_MT_POSITION_Y: dev->y = i->value; break;
          case ABS_MT_TOUCH_MAJOR: dev->width = i->value; break;
          case ABS_MT_WIDTH_MAJOR: break; // identical to ABS_MT_TOUCH_MAJOR
          case ABS_MT_TRACKING_ID: break; // TODO
          default:
            SDL_Log("Unknown EV_ABS code: %d", i->code);
        }
        break;

      case EV_KEY:
        SDL_Log("Key pressed: %d, %d", i->code, i->value);
        switch(i->code) {
          case BTN_TOUCH:
            dev->buttons = (dev->buttons & ~BIT_TOUCH) | (i->value * BIT_TOUCH);
            dev->oldX = -1;
            dev->oldY = -1;
            break;
          case BTN_STYLUS:
            dev->buttons = (dev->buttons & ~BIT_STYLUS) | (i->value * BIT_STYLUS);

            if(!i->value && dev->stylusTime > 5 && dev->stylusTime < 50) {
              colorIdx = (colorIdx + 1) % COLORS_COUNT;
            }

            dev->stylusTime = 0;
            break;
        }
        break;

      case EV_SYN:
        if(dev == &touchscreen && i->code == SYN_MT_REPORT) {
          SDL_Log("Touchscreen %d,%d %d", dev->x, dev->y, dev->width);

          float drawX, drawY, oldDrawX, oldDrawY;
          touchscreenCoordinates(dev->x, dev->y, &drawX, &drawY);
          touchscreenCoordinates(dev->oldX, dev->oldY, &oldDrawX, &oldDrawY);

          if(dev->oldX > 0 && dev->oldY > 0) {
            if(dev->width < 25) {
              // non-digitizer stylus
              blurLine(oldDrawX, oldDrawY, drawX, drawY, 20.0);
            } else {
              // finger
              deleteLine(oldDrawX, oldDrawY, drawX, drawY, 50.0);
            }
          }

          dev->oldX = dev->x;
          dev->oldY = dev->y;
        } else if(dev == &touchscreen && i->code == SYN_REPORT) {
          // ignore
        } else if(dev == &digitizer && i->code == SYN_REPORT) {
          // SDL_Log("Digitizer %d,%d @ %d", dev->x, dev->y, dev->pressure);

          float drawX, drawY, oldDrawX, oldDrawY;
          digitizerCoordinates(dev->x, dev->y, &drawX, &drawY);
          digitizerCoordinates(dev->oldX, dev->oldY, &oldDrawX, &oldDrawY);

          float dx = drawX - oldDrawX;
          float dy = drawY - oldDrawY;
          float dist = sqrtf(dx * dx + dy * dy);
          if(!dev->pressure && dev->oldPressure && dist > 4) {
            dev->pressure = dev->oldPressure;
          }

          if(dev->oldX > 0 && dev->oldY > 0 && dev->pressure > 0) {
            float effPressure;
            float width = dev->buttons & BIT_STYLUS? 5.0: 1.0;
            int structured = dev->buttons & BIT_STYLUS;

            if(dev->pressure < 300) {
              effPressure = dev->pressure * 3;
            } else if(dev->pressure < 800) {
              effPressure = 900 + (dev->pressure - 300);
            } else {
              effPressure = 900 + (dev->pressure - 300);
              width *= 1.0 + (dev->pressure - 800) / 300.0;
            }

            if(dev->pressure > 700) {
              structured = 0;
            }

            drawLine(oldDrawX, oldDrawY, drawX, drawY, effPressure / 600.0, width,
                COLORS[colorIdx].r, COLORS[colorIdx].g, COLORS[colorIdx].b,
                structured);
          }

          dev->oldX = dev->x;
          dev->oldY = dev->y;
          dev->oldPressure = dev->pressure;
          dev->pressure = 0;
        } else {
          SDL_Log("Unknown EV_SYN code: %d", i->code);
        }

        break;

      default:
        SDL_Log("Unknown i->type: %d", i->type);
        break;
    }
  }
}

void savePng(char *path) {
  for(int y = 0; y < surface->h; ++y) {
    for(int x = 0; x < surface->w; ++x) {
      uint8_t t = pixel(x, y)->r;
      pixel(x, y)->r = pixel(x, y)->b;
      pixel(x, y)->b = t;
    }
  }

  IMG_SavePNG(surface, path);

  for(int y = 0; y < surface->h; ++y) {
    for(int x = 0; x < surface->w; ++x) {
      uint8_t t = pixel(x, y)->r;
      pixel(x, y)->r = pixel(x, y)->b;
      pixel(x, y)->b = t;
    }
  }
}

int main(int argc, char **argv) {
  char *backgroundPath;
  if(argc == 2) {
    backgroundPath = argv[1];
  } else {
    backgroundPath = NULL;
  }

  if(SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
    return 1;
  }

  window = SDL_CreateWindow("write-paper",
    0, 0, 2560, 1600,
    SDL_WINDOW_FULLSCREEN | SDL_WINDOW_INPUT_GRABBED
  );
  if(!window) {
    SDL_Log("Unable to create window: %s", SDL_GetError());
    return 1;
  }

  surface = SDL_GetWindowSurface(window);
  if(!surface) {
    SDL_Log("Unable to create surface: %s", SDL_GetError());
    return 1;
  }

  if(backgroundPath) {
    backgroundImage = IMG_Load(backgroundPath);
    if(!backgroundImage) {
      SDL_Log("Unable to load %s: %s", backgroundPath, SDL_GetError());
    }
  } else {
    backgroundImage = NULL;
  }

  SDL_LockSurface(surface);
  SDL_Log("Pixel format: %s", SDL_GetPixelFormatName(surface->format->format));
  SDL_Log("Dimensions: %d x %d", surface->w, surface->h);
  for(int y = 0; y < surface->h; ++y) {
    for(int x = 0; x < surface->w; ++x) {
      struct rgba *p = pixel(x, y);
      background(p, x, y);
    }
  }
  SDL_UnlockSurface(surface);
  SDL_UpdateWindowSurface(window);

  bzero(&touchscreen, sizeof(touchscreen));
  bzero(&digitizer, sizeof(digitizer));

  touchscreen.fd = open("/dev/input/event1", O_RDONLY | O_NONBLOCK);
  digitizer.fd = open("/dev/input/event2", O_RDONLY | O_NONBLOCK);

  int updatesSinceSave = 0;
  int waitForSave = 0;
  while(1) {
    updates = 0;

    SDL_LockSurface(surface);
    process(&touchscreen);
    process(&digitizer);
    SDL_UnlockSurface(surface);

    if(updates) {
      SDL_UpdateWindowSurfaceRects(window, updateRects, updates);

      updatesSinceSave = 1;
      waitForSave = 150;
    }

    struct timespec s = {
      .tv_sec = 0,
      .tv_nsec = 1000000,
    };
    nanosleep(&s, &s);

    if(updatesSinceSave && backgroundPath) {
      if(waitForSave) {
        --waitForSave;
      } else {
        // Not too many zombies.
        while(waitpid(-1, NULL, WNOHANG) > 0);

        int child = fork();
        if(child == 0) {
          SDL_Log("Saving...");
          savePng(backgroundPath);
          SDL_Log("Saved.");
          exit(0);
        }

        if(child < 0) {
          SDL_Log("Fork for save failed.");
        }

        touchscreen.oldX = touchscreen.oldY = 0;
        digitizer.oldX = digitizer.oldY = 0;
        dropEvents(&touchscreen);
        dropEvents(&digitizer);
        updatesSinceSave = 0;
      }
    }
  }

  SDL_DestroyWindow(window);

  if(backgroundImage) SDL_FreeSurface(backgroundImage);

  SDL_Quit();
  return 0;
}
