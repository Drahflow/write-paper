#include "backlight.h"

#include "main.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

static uint16_t currentBrightness = 65534; // unlikely to be set

// 65535 == full brightness
uint8_t setBacklight(uint16_t brightness) {
  if(brightness == currentBrightness) return 1;

  int fd = open(BACKLIGHT_DEVICE, O_WRONLY);
  if(fd == -1) {
    fprintf(stderr, "Could not open backlight control file: %s", strerror(errno));
    return 0;
  }

  char buf[16];
  int len = snprintf(buf, 16, "%d\n", (brightness >> 8));
  int ret = write(fd, buf, len);
  if(ret < len) {
    fprintf(stderr, "Could not write to backlight control file: %s", strerror(errno));
    return 0;
  }

  if(close(fd) == -1) {
    fprintf(stderr, "Could not close backlight control file: %s", strerror(errno));
    return 0;
  }

  currentBrightness = brightness;
  return 1;
}
