#!/usr/bin/env python3

# Importing pigment reflectance data from doi: 10.18236/econs2.201410
# FORS Spectral Database of Historical Pigments in Different Binders
# by Antonino Cosentino

# The data as reports reflectance for cardboard above 1.0 across the
# spectrum which seems unlikely. This factor is chosen to match
# titanium white (which does not have any fluorescent properties)
# to about 63000 reflectance.
dataOverscale = 1.4

import csv

titles = []
pigments = {}
with open("FORS-Spectra-Pigments/powder.csv") as csvfile:
    data = csv.reader(csvfile)
    for row in data:
        if not titles:
            titles = row
            continue
        wavelength = float(row[0])
        frequency = 300000 / wavelength # THz

        spectrumIndex = int((frequency - 400) / 6.25)
        if spectrumIndex < 0 or spectrumIndex >= 64:
            continue

        for i in range(len(titles) - 1):
            name = titles[i + 1]
            intensity = float(row[i + 1])
            if name not in pigments:
                pigments[name] = [{
                    "count": 0,
                    "sum": 0,
                } for i in range(64)]

            pigments[name][spectrumIndex]["count"] += 1
            pigments[name][spectrumIndex]["sum"] += intensity

print("#include \"pigment_data.h\"")

for name in pigments:
    luminosity = [0 for i in range(64)]

    print()
    print("const SpectrumPigment FORS_%s = {" % name)
    print("  .bucket = {\n    ", end="")
    for i in range(64):
        reflectance = int(pigments[name][i]["sum"] / pigments[name][i]["count"] / 100.0 / dataOverscale * 65535)
        if reflectance < 0:
            reflectance = 0
        if reflectance > 65535:
            luminosity[i] = reflectance - 65535
            reflectance = 65535

        # More data is needed here...
        if name.endswith("white"):
            transmittance = reflectance * 0.05
            reflectance = reflectance * 0.95
        else:
            transmittance = reflectance * 0.75
            reflectance = reflectance * 0.25

        print("{" + str(reflectance) + "," + str(transmittance) + "," + str(luminosity[i]) + "}", end="")
        if i != 63:
            print(", ", end="")
        else:
            print()
        if i % 10 == 9:
            print("\n      ", end="")
    print("  }")
    print("};")
