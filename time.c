#define _GNU_SOURCE

#include "time.h"

#include <time.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

uint64_t nowMonotone() {
  struct timespec buf;

  int ret = clock_gettime(CLOCK_MONOTONIC, &buf);
  if(ret == -1) {
    fprintf(stderr, "Cannot get current monotone time: %s\n", strerror(errno));
    return 0;
  }

  return buf.tv_sec * 1000000000ull + buf.tv_nsec;
}
