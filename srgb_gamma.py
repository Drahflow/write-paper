#!/usr/bin/env python3

from math import exp

# As per https://en.m.wikipedia.org/wiki/SRGB
A = 0.055
Gamma = 2.4
X = 0.03928
Phi = 12.92321

print("#include \"srgb.h\"")
print("")
print("uint16_t sRGBGamma[] = {")

gamma = []
for i in range(256):
    x = i / 255.0
    if x < X:
        gamma.append(65535 * (x / Phi))
    else:
        gamma.append(65535 * pow((x + A) / (1 + A), Gamma))

for srgb in range(256):
    print(str(int(gamma[srgb] + 0.5)) + ("," if srgb != 255 else ""), end = "\n" if srgb % 30 == 0 else "")

print("};")
