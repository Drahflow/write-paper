#ifndef H_22F961C5_FB8B_45CF_86C2_CD7A0A3947AF
#define H_22F961C5_FB8B_45CF_86C2_CD7A0A3947AF

#include "layer.h"
#include "pigment.h"

#define SPECTRUM 64

typedef struct {
  // 0: 400-406.25 THz / 749.5-758 nm
  // 63: 793.75-800 THz / 377.7-374.75 nm
  struct {
    uint16_t reflectance; // 65535 == 100% reflection
    uint16_t luminosity; // 256 == 1W/m^2/nm
    // scale chosen such that single-frequency 65535 can outshine (by factor >4) sun on white
  } bucket[SPECTRUM];
} spectrum;

typedef struct {
  Layer layer;

  uint32_t blackOffset;
  uint16_t scaleFactor;
  uint8_t scaleShift;
  uint8_t *dirtyBitmap;

  spectrum data[];
} SpectrumLayer;

typedef struct SpectrumPigment {
  Pigment pigment;

  struct {
    uint16_t reflectance; // 65535 == 100% of the light reflected
    uint16_t transmittance; // 65535 == 100% of the light transmitted
    uint16_t luminosity; // 256 = 1W/m^2/nm
  } bucket[SPECTRUM];
} SpectrumPigment;

SpectrumLayer *newSpectrumLayer();
void paintSpectrumPigment(SpectrumLayer *dst, uint16_t x, uint16_t y,
    const SpectrumPigment *src, uint16_t thickness, uint16_t coverage);

void mixSpectrumPigments(SpectrumPigment *dst, const SpectrumPigment *src, uint16_t strength);
void diluteSpectrumPigment(SpectrumPigment *dst, uint16_t strength);

#endif
