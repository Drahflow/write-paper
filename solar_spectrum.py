#!/usr/bin/env python3

import csv

spectrum = [{"count": 0, "sum": 0} for x in range(64)]

with open("AM0AM1_5.csv") as csvfile:
    solar = csv.reader(csvfile)
    for row in solar:
        if row[0].startswith("ASTM"):
            continue
        if row[0].startswith("Wavelength"):
            continue
        wavelength = float(row[0]) # nm
        intensity = row[1]

        frequency = 300000 / wavelength # THz

        spectrumIndex = int((frequency - 400) / 6.25)
        if spectrumIndex < 0 or spectrumIndex >= len(spectrum):
            continue

        spectrum[spectrumIndex]["count"] += 1
        spectrum[spectrumIndex]["sum"] += float(intensity)

for i in range(64):
    print("Spectrum[", i, "]: ", int(spectrum[i]["sum"] / spectrum[i]["count"] * 10000))
