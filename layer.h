#ifndef H_96274CD0_A8C5_4BE4_A2E2_52631A7088E3
#define H_96274CD0_A8C5_4BE4_A2E2_52631A7088E3

#include <stdint.h>

struct SpectrumPigment;
struct rgba;

typedef struct Layer {
  void (*renderToFramebuffer)(struct Layer *, uint32_t x, uint32_t y, uint32_t w, uint32_t h);
  void (*renderSpectrumPigment)(struct Layer *, const struct SpectrumPigment *, struct rgba *dstWhite, struct rgba *dstBlack);
} Layer;

#endif
