#ifndef H_51B56102_571D_448C_AA7D_278491F6C3CC
#define H_51B56102_571D_448C_AA7D_278491F6C3CC

#include "spectrum.h"

extern const SpectrumPigment CODE_pitchblack;
extern const SpectrumPigment CODE_red;
extern const SpectrumPigment CODE_yellow;
extern const SpectrumPigment CODE_green;
extern const SpectrumPigment CODE_cyan;
extern const SpectrumPigment CODE_blue;
extern const SpectrumPigment CODE_magenta;
extern const SpectrumPigment CODE_neon_red;
extern const SpectrumPigment CODE_neon_yellow;
extern const SpectrumPigment CODE_neon_green;
extern const SpectrumPigment CODE_neon_cyan;
extern const SpectrumPigment CODE_neon_blue;
extern const SpectrumPigment CODE_neon_magenta;

extern const SpectrumPigment FORS_burntumber;
extern const SpectrumPigment FORS_rawumber;
extern const SpectrumPigment FORS_vandykebrown;
extern const SpectrumPigment FORS_burntsienna;
extern const SpectrumPigment FORS_rawsienna;
extern const SpectrumPigment FORS_redochre;
extern const SpectrumPigment FORS_redlead;
extern const SpectrumPigment FORS_cadmiumred;
extern const SpectrumPigment FORS_alizarin;
extern const SpectrumPigment FORS_madderlake;
extern const SpectrumPigment FORS_lacdye;
extern const SpectrumPigment FORS_carminelake;
extern const SpectrumPigment FORS_vermilion;
extern const SpectrumPigment FORS_realgar;
extern const SpectrumPigment FORS_yellowlakeresed;
extern const SpectrumPigment FORS_massicot;
extern const SpectrumPigment FORS_yellowochre;
extern const SpectrumPigment FORS_gamboge;
extern const SpectrumPigment FORS_naplesyellow;
extern const SpectrumPigment FORS_leadtinyellowII;
extern const SpectrumPigment FORS_leadtinyellowI;
extern const SpectrumPigment FORS_saffron;
extern const SpectrumPigment FORS_orpiment;
extern const SpectrumPigment FORS_cobaltyellow;
extern const SpectrumPigment FORS_cadmiumyellow;
extern const SpectrumPigment FORS_chromegreen;
extern const SpectrumPigment FORS_cobaltgreen;
extern const SpectrumPigment FORS_cadmiumgreen;
extern const SpectrumPigment FORS_greenearth;
extern const SpectrumPigment FORS_viridian;
extern const SpectrumPigment FORS_phthalogreen;
extern const SpectrumPigment FORS_verdigris;
extern const SpectrumPigment FORS_malachite;
extern const SpectrumPigment FORS_bluebice;
extern const SpectrumPigment FORS_cobaltblue;
extern const SpectrumPigment FORS_azurite;
extern const SpectrumPigment FORS_egyptianblue;
extern const SpectrumPigment FORS_ultramarine;
extern const SpectrumPigment FORS_Phthaloblue;
extern const SpectrumPigment FORS_smalt;
extern const SpectrumPigment FORS_indigo;
extern const SpectrumPigment FORS_mayablue;
extern const SpectrumPigment FORS_prussianblue;
extern const SpectrumPigment FORS_cobaltviolet;
extern const SpectrumPigment FORS_ivoryblack;
extern const SpectrumPigment FORS_vineblack;
extern const SpectrumPigment FORS_boneblack;
extern const SpectrumPigment FORS_lampblack;
extern const SpectrumPigment FORS_gypsum;
extern const SpectrumPigment FORS_chalk;
extern const SpectrumPigment FORS_leadwhite;
extern const SpectrumPigment FORS_zincwhite;
extern const SpectrumPigment FORS_titaniumwhite;
extern const SpectrumPigment FORS_lithopone;
extern const SpectrumPigment FORS_cardboard;

#endif
