#!/usr/bin/env python3

from math import exp

# As per https://en.m.wikipedia.org/wiki/SRGB
A = 0.055
Gamma = 2.4
X = 0.03928
Phi = 12.92321

print("#include \"srgb.h\"")
print("")
print("uint8_t sRGBGammaInverse[] = {")

gamma = []
for i in range(256):
    x = i / 255.0
    if x < X:
        gamma.append(65535 * (x / Phi))
    else:
        gamma.append(65535 * pow((x + A) / (1 + A), Gamma))

for linear in range(65536):
    mid = int(len(gamma) / 2)
    step = int(mid / 2)

    for i in range(15):
        if gamma[mid] < linear:
            mid += step
        else:
            mid -= step
        step = int(step / 2)

    while mid > 0 and abs(gamma[mid - 1] - linear) < abs(gamma[mid] - linear):
        mid = mid - 1
    while mid < len(gamma) - 1 and abs(gamma[mid + 1] - linear) < abs(gamma[mid] - linear):
        mid = mid + 1

    print(str(mid) + ("," if linear != 65535 else ""), end = "\n" if linear % 30 == 0 else "")

print("};")
