#!/usr/bin/env python3

import csv

spectrum = [{"count": 0, "l": 0, "m": 0, "s": 0} for x in range(64)]

with open("linss2_10e_5.csv") as csvfile:
    solar = csv.reader(csvfile)
    for row in solar:
        wavelength = float(row[0]) # nm
        l_intensity = row[1]
        m_intensity = row[2]
        s_intensity = row[3]

        frequency = 300000 / wavelength # THz

        spectrumIndex = int((frequency - 400) / 6.25)
        if spectrumIndex < 0 or spectrumIndex >= len(spectrum):
            continue

        spectrum[spectrumIndex]["count"] += 1
        spectrum[spectrumIndex]["l"] += float(l_intensity or 0)
        spectrum[spectrumIndex]["m"] += float(m_intensity or 0)
        spectrum[spectrumIndex]["s"] += float(s_intensity or 0)

for i in range(64):
    if spectrum[i]["count"] == 0:
        spectrum[i]["count"] = 1
        if i < 63:
            spectrum[i]["s"] = (spectrum[i-1]["s"] + spectrum[i+1]["s"]) / 2
            spectrum[i]["m"] = (spectrum[i-1]["m"] + spectrum[i+1]["m"]) / 2
            spectrum[i]["l"] = (spectrum[i-1]["l"] + spectrum[i+1]["l"]) / 2

for i in range(64):
    print("S_Cone[", i, "]: ", int(spectrum[i]["s"] / spectrum[i]["count"] * 65535))

for i in range(64):
    print("M_Cone[", i, "]: ", int(spectrum[i]["m"] / spectrum[i]["count"] * 65535))

for i in range(64):
    print("L_Cone[", i, "]: ", int(spectrum[i]["l"] / spectrum[i]["count"] * 65535))
