#include "digitizer.h"

#include "main.h"
#include "memory.h"
#include "time.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>

#include <linux/input.h>

#define BUTTON_TOUCH 1
#define BUTTON_STYLUS 2

Digitizer *newDigitizer() {
  Digitizer *digitizer = assertMalloc(sizeof(Digitizer));
  digitizer->fd = open(DIGITIZER_DEVICE, O_RDONLY | O_NONBLOCK);
  if(digitizer->fd < 0) {
    fprintf(stderr, "Could not open digitizer device: %s\n", strerror(errno));
  }

  digitizer->oldX = ~0u;
  digitizer->oldY = ~0u;
  digitizer->oldButtons = 0;
  digitizer->oldPressure = 0;
  digitizer->pressure = 0;

  digitizer->tool = NULL;
  digitizer->ui = NULL;

  return digitizer;
}

static struct input_event buffer[4096];

#ifdef EXCITE_WRITE
static void digitizerCoordinates(int x, int y, float *screenX, float *screenY) {
  *screenX = (x - 91.0) * FRAMEBUFFER_WIDTH / (21658 + 91);
  *screenY = (y + 10.0) * FRAMEBUFFER_HEIGHT / (13536);
}
#elif GN5510
static void digitizerCoordinates(int x, int y, float *screenX, float *screenY) {
  *screenY = FRAMEBUFFER_HEIGHT - (x + 0.0) * FRAMEBUFFER_HEIGHT / (10800);
  *screenX = (y - 200.0) * FRAMEBUFFER_WIDTH / (17000);
}
#endif

void digitizerInput(Digitizer *digitizer) {
  int ret = read(digitizer->fd, buffer, sizeof(buffer));
  if(ret == -1) return;

  struct input_event *end = (struct input_event *)((char *)buffer + ret);
  for(struct input_event *i = buffer; i != end; ++i) {
    switch(i->type) {
      case EV_ABS:
        switch(i->code) {
          // Digitizer
          case ABS_X: digitizer->x = i->value; break;
          case ABS_Y: digitizer->y = i->value; break;
          case ABS_PRESSURE: digitizer->pressure = i->value; break;
          default:
            printf("Unknown EV_ABS code on digitizer: %d", i->code);
        }
        break;

      case EV_KEY:
        printf("Key pressed: %d, %d\n", i->code, i->value);
        switch(i->code) {
          case BTN_TOUCH:
            digitizer->buttons = (digitizer->buttons & ~BUTTON_TOUCH) | (i->value * BUTTON_TOUCH);
            digitizer->oldX = ~0u;
            digitizer->oldY = ~0u;

            float x, y;
            digitizerCoordinates(digitizer->x, digitizer->y, &x, &y);

            if(digitizer->buttons & BUTTON_TOUCH) {
              if(digitizer->tool) digitizer->tool->lower(digitizer->tool, x, y);
            } else {
              if(digitizer->tool) digitizer->tool->raise(digitizer->tool, x, y);
              digitizer->oldPressure = 0;
            }
            break;
          case BTN_STYLUS: {
            digitizer->buttons = (digitizer->buttons & ~BUTTON_STYLUS) | (i->value * BUTTON_STYLUS);

            float x, y;
            digitizerCoordinates(digitizer->x, digitizer->y, &x, &y);

            if(!(BUTTON_STYLUS & digitizer->oldButtons) && (BUTTON_STYLUS & digitizer->buttons)) {
              digitizer->ui->btnDown(digitizer->ui, x, y);
            }
            if((BUTTON_STYLUS & digitizer->oldButtons) && !(BUTTON_STYLUS & digitizer->buttons)) {
              digitizer->ui->btnUp(digitizer->ui, x, y);
            }

            digitizer->oldButtons = digitizer->buttons;
            break;
          }
        }
        break;

      case EV_SYN:
        if(i->code == SYN_REPORT) {
          // printf("Digitizer %d,%d @ %d", digitizer->x, digitizer->y, digitizer->pressure);

          float drawX, drawY, oldDrawX, oldDrawY;
          digitizerCoordinates(digitizer->x, digitizer->y, &drawX, &drawY);
          digitizerCoordinates(digitizer->oldX, digitizer->oldY, &oldDrawX, &oldDrawY);

          float dx = drawX - oldDrawX;
          float dy = drawY - oldDrawY;
          float dist = sqrtf(dx * dx + dy * dy);
          if(!digitizer->pressure && digitizer->oldPressure && dist > 4) {
            digitizer->pressure = digitizer->oldPressure;
          }

          uint64_t now = nowMonotone();

          if(digitizer->oldX != ~0u && digitizer->oldY != ~0u && digitizer->pressure > 0) {
            // if(digitizer->pressure < 300) {
            //   effPressure = digitizer->pressure * 3;
            // } else {
            //   effPressure = 900 + (digitizer->pressure - 300);
            // }

            uint32_t effPressure = digitizer->pressure << 6;
            if(digitizer->tool) {
              digitizer->tool->draw(digitizer->tool, oldDrawX, oldDrawY, drawX, drawY,
                  effPressure, now - digitizer->lastDraw);
            }
          }

          digitizer->oldX = digitizer->x;
          digitizer->oldY = digitizer->y;
          digitizer->oldPressure = digitizer->pressure;
          digitizer->lastDraw = now;
          digitizer->pressure = 0;
        } else {
          printf("Unknown EV_SYN code: %d", i->code);
        }

        break;

      default:
        printf("Unknown i->type: %d", i->type);
        break;
    }
  }
}
