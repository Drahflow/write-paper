#ifndef H_7E9A8BA1_72CB_4198_B9F2_EF0BE820BF65
#define H_7E9A8BA1_72CB_4198_B9F2_EF0BE820BF65

#ifndef CONFIG
#error Must define CONFIG to include in Makefile
#endif

#include CONFIG

#include <stdint.h>

#ifdef FRAMEBUFFER_RGBA
typedef struct __attribute__((packed)) rgba {
  uint8_t r, g, b, a;
} rgba;
#endif

#ifdef FRAMEBUFFER_BGRA
typedef struct __attribute__((packed)) rgba {
  uint8_t b, g, r, a;
} rgba;
#endif

// The actual, device framebuffer
extern rgba *framebuffer;

// A copy of the drawing as seen in the framebuffer, for restore after UI.
extern rgba *drawingBuffer;

extern uint16_t paintGeneration[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT];

#endif
