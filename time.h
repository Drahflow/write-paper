#ifndef H_76A41085_C26D_42C9_9980_D931D361C8F4
#define H_76A41085_C26D_42C9_9980_D931D361C8F4

#include <stdint.h>

// Nanoseconds, monotone time.
uint64_t nowMonotone();

#endif
