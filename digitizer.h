#ifndef H_894D939C_8EB0_4137_962D_18C3FE714B65
#define H_894D939C_8EB0_4137_962D_18C3FE714B65

#include <stdint.h>

#include "tool.h"
#include "ui.h"

typedef struct {
  int fd;
  uint32_t buttons, oldButtons;
  uint32_t pressure, oldPressure;
  uint32_t x, oldX;
  uint32_t y, oldY;
  uint64_t lastDraw;

  Tool *tool;
  UI *ui;
} Digitizer;

Digitizer *newDigitizer();
void digitizerInput(Digitizer *);

#endif
