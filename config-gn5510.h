#ifndef H_F41B833F_3669_4687_8619_C2214B278A86
#define H_F41B833F_3669_4687_8619_C2214B278A86

#define WITHPNG
#define FRAMEBUFFER_BGRA
#define FRAMEBUFFER_DEVICE "/dev/graphics/fb0"
#define FRAMEBUFFER_WIDTH 1280
#define FRAMEBUFFER_HEIGHT 800
#define TOUCHSCREEN_DEVICE "/dev/input/event1"
#define DIGITIZER_DEVICE "/dev/input/event9"
#define BACKLIGHT_DEVICE "/sys/class/backlight/panel/brightness"

#define GN5510 1

#endif
