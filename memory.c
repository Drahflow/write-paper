#include "memory.h"

#include <assert.h>

void *assertMalloc(size_t size) {
  void *ret = malloc(size);
  assert(ret);

  return ret;
}
