#ifndef H_2CD0E7EB_B3F4_404C_8D0B_4BF55F7A5C8D
#define H_2CD0E7EB_B3F4_404C_8D0B_4BF55F7A5C8D

#include <stdint.h>

#include "tool.h"
#include "ui.h"

#define TOUCHSCREEN_TRACKS 16

typedef struct {
  int fd;
  uint32_t trackX, trackY, trackWidth;
  uint8_t tracking[TOUCHSCREEN_TRACKS], oldTracking[TOUCHSCREEN_TRACKS];
  uint32_t x[TOUCHSCREEN_TRACKS], oldX[TOUCHSCREEN_TRACKS];
  uint32_t y[TOUCHSCREEN_TRACKS], oldY[TOUCHSCREEN_TRACKS];
  uint32_t width[TOUCHSCREEN_TRACKS], oldWidth[TOUCHSCREEN_TRACKS];
  uint64_t lastDraw[TOUCHSCREEN_TRACKS];

  Tool *fingerTool[4];
  Tool *stylusTool[4];
} Touchscreen;

Touchscreen *newTouchscreen();
void touchscreenInput(Touchscreen *);

#endif
